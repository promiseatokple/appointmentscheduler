package com.promise.salonapplication.customer;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.promise.salonapplication.R;
import com.promise.salonapplication.models.ChildList;
import com.promise.salonapplication.models.MyChildViewHolder;
import com.promise.salonapplication.models.MyParentViewHolder;
import com.promise.salonapplication.models.ParentList;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/*public class CategoriesActivity extends AppCompatActivity {


    SparseArray<Group> groups = new SparseArray<Group>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);


        createData();
        ExpandableListView listView = (ExpandableListView) findViewById(R.id.listView);
        MyExpandableListAdapter adapter = new MyExpandableListAdapter(this,
                groups);
        listView.setAdapter(adapter);
    }

    public void createData() {
        for (int j = 0; j < 5; j++) {
            Group group = new Group("Test " + j);
            for (int i = 0; i < 5; i++) {
                group.children.add("Sub Item" + i);
            }
            groups.append(j, group);
        }
    }
}*/



public class CategoriesActivity extends AppCompatActivity {

    @BindView(R.id.recycler_Expand)
    RecyclerView recycler_Expand;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        ButterKnife.bind(this);

        //Define recycleview

        recycler_Expand.setLayoutManager(new LinearLayoutManager(this));

        //Initialize your Firebase app
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Services");

        // Reference to your entire Firebase database
        DatabaseReference parentReference = database.getReference();

        //reading data from firebase
        parentReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<ParentList> Parent = new ArrayList<>();
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){


                    final String ParentKey = snapshot.getKey();
                    final String name = String.valueOf(snapshot.child("name").getValue());
                    Log.d("ParentKey", ParentKey);

//                    snapshot.child("titre").getValue();
                    String key = snapshot.getKey();
                    snapshot.child(name).getValue();


                    String display_name = String.valueOf(dataSnapshot.child("displayName").getValue());
//            String email_address = String.valueOf(dataSnapshot.child("emailAddress").getValue());
//            String phone_number = String.valueOf(dataSnapshot.child("phoneNumber").getValue());
//            String profile_image = String.valueOf(dataSnapshot.child("profileImage").getValue());
//
                    DatabaseReference childReference;
                        childReference = parentReference.getRef();
                        childReference.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                final List<ChildList> Child = new ArrayList<>();
                                //numberOnline = 0;
                                DataSnapshot snap = dataSnapshot;

                                for (DataSnapshot snapshot1:dataSnapshot.getChildren())
                                {
                                    final String ChildValue = String.valueOf(snapshot1.getChildren().iterator().next());



                                    DataSnapshot values = dataSnapshot.child(ChildValue);
                                    final String name = String.valueOf(values.child(ParentKey).getChildren());
                                    DatabaseReference dr = database.getReference().child(ChildValue);

                                    dr.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            String price = String.valueOf(snap.child("price").getValue());
                                            Log.d("PRICE", price);
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

//                                    final String name = String.valueOf(snapshot1.child(ChildValue));
                                    final String price = String.valueOf(snapshot1.child("price").getValue());
                                    final String duration = String.valueOf(snapshot1.child("duration").getValue());

                                    Log.w("ChildValue", ChildValue);
                                    Log.w("NAME", name);


//                                snapshot1.child("titre").getValue();
                                    snapshot1.child("name").getValue();

//                                Child.add(new ChildList(ChildValue));
                                    Child.add(new ChildList(ChildValue));

                                }

                                Parent.add(new ParentList(ParentKey, Child));

                                DocExpandableRecyclerAdapter adapter = new DocExpandableRecyclerAdapter(Parent);

                                recycler_Expand.setAdapter(adapter);

                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                // Failed to read value
                                System.out.println("Failed to read value." + error.toException());
                            }

                        });}}



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



    public class DocExpandableRecyclerAdapter extends ExpandableRecyclerViewAdapter<MyParentViewHolder,MyChildViewHolder> {


        public DocExpandableRecyclerAdapter(List<ParentList> groups) {
            super(groups);
        }

        @Override
        public MyParentViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parent, parent, false);
            return new MyParentViewHolder(view);
        }

        @Override
        public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_child, parent, false);
            return new MyChildViewHolder(view);
        }

        @Override
        public void onBindChildViewHolder(MyChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {

            final ChildList childItem = ((ParentList) group).getItems().get(childIndex);
            holder.onBind(childItem.getName());
           /* holder.onBind(childItem.getDuration());
            holder.onBind(childItem.getItemPrice());*/
            final String TitleChild=group.getTitle();
            holder.listChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast toast = Toast.makeText(getApplicationContext(), TitleChild, Toast.LENGTH_SHORT);
                    toast.show();
                }

            });

        }

        @Override
        public void onBindGroupViewHolder(MyParentViewHolder holder, int flatPosition, final ExpandableGroup group) {
            holder.setParentTitle(group);

            if(group.getItems()==null)
            {
                holder.listGroup.setOnClickListener(  new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Toast toast = Toast.makeText(getApplicationContext(), group.toString(), Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });

            }
        }


    }

}
