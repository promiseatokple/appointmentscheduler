package com.promise.salonapplication.customer.multiselect;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.promise.salonapplication.R;


public class SalonServicesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_services);

        Button multiSelect = (Button) findViewById(R.id.multi_check_button);
        multiSelect.setOnClickListener(navigateTo(MultiCheckActivity.class));




    }

    public View.OnClickListener navigateTo(final Class<?> clazz) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalonServicesActivity.this, clazz);
                startActivity(intent);
            }
        };
    }
}
