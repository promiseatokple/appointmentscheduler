package com.promise.salonapplication.customer.implementdemo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.promise.salonapplication.R;
import com.promise.salonapplication.customer.demo.constant.Constant;
import com.promise.salonapplication.customer.implementdemo.adapter.SalonCartItemAdapter;
import com.promise.salonapplication.customer.implementdemo.model.SalonCart;
import com.promise.salonapplication.customer.implementdemo.model.SalonCartItem;
import com.promise.salonapplication.customer.implementdemo.model.SalonService;
import com.promise.salonapplication.customer.implementdemo.util.SalonCartHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SalonShoppingCartActivity extends AppCompatActivity {
    private static final String TAG = "ShoppingCartActivity";

    ListView lvCartItems;
    Button bClear;
    Button bShop;
    TextView tvTotalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_activity_cart);

        lvCartItems = (ListView) findViewById(R.id.lvCartItems);
        LayoutInflater layoutInflater = getLayoutInflater();

        final SalonCart cart = SalonCartHelper.getCart();
        final TextView tvTotalPrice = (TextView) findViewById(R.id.tvTotalPrice);
        tvTotalPrice.setText(SalonConstant.CURRENCY+String.valueOf(cart.getTotalPrice().setScale(2, BigDecimal.ROUND_HALF_UP)));

        lvCartItems.addHeaderView(layoutInflater.inflate(R.layout.demo_cart_header, lvCartItems, false));

        final SalonCartItemAdapter cartItemAdapter = new SalonCartItemAdapter(this);
        cartItemAdapter.updateCartItems(getCartItems(cart));

        lvCartItems.setAdapter(cartItemAdapter);

        bClear = (Button) findViewById(R.id.bClear);
        bShop = (Button) findViewById(R.id.bShop);

        bClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Clearing the shopping cart");
                cart.clear();
                cartItemAdapter.updateCartItems(getCartItems(cart));
                cartItemAdapter.notifyDataSetChanged();
                tvTotalPrice.setText(Constant.CURRENCY+String.valueOf(cart.getTotalPrice().setScale(2, BigDecimal.ROUND_HALF_UP)));
            }
        });

        bShop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalonShoppingCartActivity.this, DemoMainActivity.class);
                startActivity(intent);
            }
        });

        lvCartItems.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(SalonShoppingCartActivity.this)
                        .setTitle(getResources().getString(R.string.delete_item))
                        .setMessage(getResources().getString(R.string.delete_item_message))
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                List<SalonCartItem> cartItems = getCartItems(cart);
                                cart.remove(cartItems.get(position-1).getProduct());
                                cartItems.remove(position-1);
                                cartItemAdapter.updateCartItems(cartItems);
                                cartItemAdapter.notifyDataSetChanged();
                                tvTotalPrice.setText(Constant.CURRENCY+String.valueOf(cart.getTotalPrice().setScale(2, BigDecimal.ROUND_HALF_UP)));
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.no), null)
                        .show();
                return false;
            }
        });

        lvCartItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                List<SalonCartItem> cartItems = getCartItems(cart);
                SalonService product = cartItems.get(position-1).getProduct();
                Log.d(TAG, "Viewing service: " + product.getName());
                bundle.putSerializable("service", product);
                Intent intent = new Intent(SalonShoppingCartActivity.this, SalonProductActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private List<SalonCartItem> getCartItems(SalonCart cart) {
        List<SalonCartItem> cartItems = new ArrayList<>();
        Log.d(TAG, "Current shopping cart: " + cart.getProducts());

        Map<SalonSaleable, Integer> itemMap = cart.getItemWithQuantity();

        for (Entry<SalonSaleable, Integer> entry : itemMap.entrySet()) {
            SalonCartItem cartItem = new SalonCartItem();
            cartItem.setProduct((SalonService) entry.getKey());
            cartItem.setQuantity(entry.getValue());
            cartItems.add(cartItem);
        }

        Log.d(TAG, "Cart item list: " + cartItems.toArray());
        return cartItems;
    }
}
