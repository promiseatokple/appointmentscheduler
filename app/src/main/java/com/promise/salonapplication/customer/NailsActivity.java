package com.promise.salonapplication.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.promise.salonapplication.Common.Service;
import com.promise.salonapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NailsActivity  extends AppCompatActivity {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    View view;
    String category_name;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout. activity_nails);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null) {
            category_name = bundle.getString("category_name");
        }

        progressBar = findViewById(R.id.progress_bar);
        ButterKnife.bind(this);
        init();
        getNails();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.single_nail, container, false);

        return view;
    }


    private void init(){
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        db = FirebaseFirestore.getInstance();
    }

    private void getNails(){
        Query query = db.collection("Services").document("Category").collection("Nail");

        FirestoreRecyclerOptions<Service> beauticianRoptions = new FirestoreRecyclerOptions.Builder<Service>()
                .setQuery(query, Service.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Service, NailsHolder>(beauticianRoptions) {
            @Override

            @Nullable
            public void onBindViewHolder(@NonNull NailsHolder holder, int position, @NonNull Service model) {
                progressBar.setVisibility(View.GONE);
                holder.tv_service_name.setText(model.getName());
                holder.tv_service_price.setText(String.valueOf(model.getPrice()));
                holder.tv_service_duration.setText(model.getDuration());



                holder.itemView.setOnClickListener(v -> {
                    Snackbar.make(recyclerView, model.getName(),  Snackbar.LENGTH_LONG)
                            .setAction("Viewing Item ..", null).show();
//                    getSupportFragmentManager().beginTransaction().replace(R.id.services_container, new SkincareServicesFragment()).commit();
                    Intent intent = new Intent(NailsActivity.this, ChooseBeauticianActivity.class);
                    intent.putExtra("serviceName", model.getName());
                    intent.putExtra("price", holder.tv_service_price.getText());
                    intent.putExtra("duration", model.getDuration());
                    intent.putExtra("category_name", category_name);
                    startActivity(intent);
                });
            }

            @Override
            public NailsHolder onCreateViewHolder(@NonNull ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.single_nail, group, false);

                return new NailsHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e("error", e.getMessage());
            }
        };

        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }


    public class NailsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_service_name)
        TextView tv_service_name;
        @BindView(R.id.tv_service_price)
        TextView tv_service_price;
        @BindView(R.id.tv_service_duration)
        TextView tv_service_duration;


        public NailsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tv_service_duration = itemView.findViewById(R.id.tv_service_duration);
            tv_service_price = itemView.findViewById(R.id.tv_service_price);
            tv_service_name = itemView.findViewById(R.id.tv_service_name);

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}
