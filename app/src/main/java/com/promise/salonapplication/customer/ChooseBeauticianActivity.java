package com.promise.salonapplication.customer;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.promise.salonapplication.Common.Beautician;
import com.promise.salonapplication.Common.Service;
import com.promise.salonapplication.R;
import com.promise.salonapplication.services.MyAppsNotificationManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class ChooseBeauticianActivity extends AppCompatActivity {
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    AlertDialog alertDialog;



//    @BindView(R.id.search_beautician)
//    SearchView search_beautician;



    public String service_name, duration, price, category_name, time, selected_services;
    public Bundle mBundle;

    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter adapter;
    LinearLayoutManager linearLayoutManager;
 MyAppsNotificationManager myAppsNotificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beauticians);
        myAppsNotificationManager = new MyAppsNotificationManager(this);

        myAppsNotificationManager.registerNotificationChannel(
                getString(R.string.NEWS_CHANNEL_ID),
                getString(R.string.CHANNEL_NEWS),
                getString(R.string.CHANNEL_DESCRIPTION));


        alertDialog = new  SpotsDialog.Builder().setContext(ChooseBeauticianActivity.this).setMessage("Getting Available Beauticians").setCancelable(true).setCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        }).build();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
          service_name = bundle.getString("serviceName");
          price =  bundle.getString("price");
          selected_services = bundle.getString("selected_services");
          time = bundle.getString("time");
          duration =  bundle.getString("duration");
            category_name =  bundle.getString("category_name");

            Toast.makeText(this, bundle.getString("serviceName"), Toast.LENGTH_LONG).show();

            bundle.putString("selected_services", selected_services);
            bundle.putString("serviceName", service_name);
            bundle.putString("category_name", category_name);
            bundle.putString("time", time);
        }


        // TODO: 6/15/19 Work on this pending intent
//        Intent intent = new Intent(this, CustomerMapsActivity.class);
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);






        ButterKnife.bind(this);
        init();
        getBeauticians();

    }

    private void init(){
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        db = FirebaseFirestore.getInstance();
    }

    private void getBeauticians(){
        Query query = db.collection("Beauticians");

        FirestoreRecyclerOptions<Beautician> firestoreRecyclerOptions = new FirestoreRecyclerOptions.Builder<Beautician>()
                .setQuery(query, Beautician.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Beautician, BeauticiansHolder>(firestoreRecyclerOptions) {
            @Override
            public void onBindViewHolder(@NonNull BeauticiansHolder holder, int position,@NonNull  Beautician model) {
                progressBar.setVisibility(View.GONE);
                alertDialog.dismiss();

                Service service = new Service();
                String beautician_number = model.getPhoneNumber();
                holder.tv_beautician_name.setText(model.getName());
                holder.rb_beautician_rating.setRating(3);
                holder.tv_name_of_salon.setText(model.getNameOfSalon());

//                if(model.getProfileImage() != null && holder.itemView.findViewById(R.id.imageView) != null){
                    //Picasso.get().load(model.getProfileImage()).placeholder(R.drawable.default_user).fit().into(holder.imageView);
                Picasso.get().load(model.getProfileImage()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_user).fit().into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(model.getProfileImage()).placeholder(R.drawable.default_user).fit().into(holder.imageView);
                    }
                });

//                }
//                else{
//                    Glide.with(getApplicationContext())
//                            .load(model.getProfileImage())
//                            .into(holder.imageView);

//                    holder.imageView.setImageResource(R.drawable.big_pic);
//                }



                holder.itemView.setOnClickListener(v -> {

                    Snackbar.make(recyclerView, ", ", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                        Log.d("BEAUTICIAN ID: ",String.valueOf( model.getName()));
//                    getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container, new ConfirmBookingFragment()).commit();


                    final String beautician_id = getSnapshots().getSnapshot(position).getId();
                    //todo; changed intent intention, review.
                    Intent intent = new Intent(ChooseBeauticianActivity.this, ConfirmRequestActivity.class);
                    intent.putExtra("beautician_name", model.getName());
                    intent.putExtra("nameOfSalon",model.getNameOfSalon());
                    intent.putExtra("priceInSummary",price);
                    intent.putExtra("category_name", category_name);
                    intent.putExtra("serviceName",service_name);
                    intent.putExtra("duration", duration);
                    intent.putExtra("time", time);
                    intent.putExtra("selected_services", selected_services);

                    intent.putExtra("beautician_id", beautician_id);
                    intent.putExtra("beautician_number", beautician_number);
///Beauticians/BMDED6ooldlu4NKVd1zJ
                    Log.w("BeauticianID:" , beautician_id);

                    startActivity(intent);




                });


            }

            @Override
            public BeauticiansHolder onCreateViewHolder(ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.single_beautician, group, false);




                return new BeauticiansHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e("error", e.getMessage());
            }
        };

        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }




    public class BeauticiansHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_beautician_name)
        TextView tv_beautician_name;
        @BindView(R.id.imageView)
        CircleImageView imageView;
        @BindView(R.id.nameOfSalon)
        TextView tv_name_of_salon;
        @BindView(R.id.rb_beautician_rating)
        RatingBar rb_beautician_rating;



        public BeauticiansHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }




}
