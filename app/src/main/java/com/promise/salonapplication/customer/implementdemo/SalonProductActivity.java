package com.promise.salonapplication.customer.implementdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.promise.salonapplication.R;
import com.promise.salonapplication.customer.implementdemo.model.SalonCart;
import com.promise.salonapplication.customer.implementdemo.util.SalonCartHelper;
import com.promise.salonapplication.customer.multiselect.SalonServiceOld;

public class SalonProductActivity extends AppCompatActivity {
    private static final String TAG = "ProductActivity";

    TextView tvServiceName;
    TextView tvProductDesc;
    ImageView ivProductImage;
    Spinner spQuantity;
    Button bOrder;
    SalonServiceOld service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.demo_activity_product);

        Bundle data = getIntent().getExtras();
        service = (SalonServiceOld) data.getSerializable("service");

        Log.d(TAG, "Service hashCode: " + service.hashCode());

        //Set Shopping Cart link
        setShoppingCartLink();

        //Retrieve views
        retrieveViews();

        //Set service properties
        setProductProperties();

        //Initialize quantity
        initializeQuantity();

        //On ordering of service
        onOrderProduct();
    }

    private void setShoppingCartLink() {
        TextView tvViewShoppingCart = (TextView)findViewById(R.id.tvViewShoppingCart);
        SpannableString content = new SpannableString(getText(R.string.shopping_cart));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvViewShoppingCart.setText(content);

    }

    private void retrieveViews() {
        tvServiceName = (TextView) findViewById(R.id.tvProductName);
        tvProductDesc = (TextView) findViewById(R.id.tvProductDesc);
        ivProductImage = (ImageView) findViewById(R.id.ivProductImage);
        spQuantity = (Spinner) findViewById(R.id.spQuantity);
        bOrder = (Button) findViewById(R.id.bOrder);
    }

    private void setProductProperties() {
        tvServiceName.setText(service.getName());
//        tvProductDesc.setText(service.getDescription());
//        ivProductImage.setImageResource(this.getResources().getIdentifier(service.getImageName(), "drawable", this.getPackageName()));
    }

    private void initializeQuantity() {
        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, SalonConstant.QUANTITY_LIST);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spQuantity.setAdapter(dataAdapter);
    }

    private void onOrderProduct() {
        bOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SalonCart cart = SalonCartHelper.getCart();
                Log.d(TAG, "Adding service: " + service.getName());
                //cart.add(service, Integer.valueOf(spQuantity.getSelectedItem().toString()));
                Intent intent = new Intent(SalonProductActivity.this, SalonShoppingCartActivity.class);
                startActivity(intent);
            }
        });
    }
}
