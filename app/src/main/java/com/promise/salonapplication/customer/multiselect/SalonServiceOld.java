package com.promise.salonapplication.customer.multiselect;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class SalonServiceOld implements Parcelable, Serializable {

    private String name;
    private boolean isFavorite;
    private double price;
    private int duration;

    public SalonServiceOld(String name, boolean isFavorite, double price, int duration) {
        this.name = name;
        this.isFavorite = isFavorite;
        this.price = price;
        this.duration = duration;
    }

    public SalonServiceOld() {
        this.name = "";
        this.price = 0;
        this.duration = 1;
    }

    public SalonServiceOld(String name, double price, int duration) {
        this.name = name;
        this.price = price;
        this.duration = duration;
    }

    protected SalonServiceOld(Parcel in) {
        name = in.readString();
        isFavorite = in.readByte() != 0;
        if (in.readByte() == 0) {
            price = 0;
        } else {
            price = in.readLong();
        }
        duration = in.readInt();
    }


    public static final Creator<SalonServiceOld> CREATOR = new Creator<SalonServiceOld>() {
        @Override
        public SalonServiceOld createFromParcel(Parcel in) {
            return new SalonServiceOld(in);
        }

        @Override
        public SalonServiceOld[] newArray(int size) {
            return new SalonServiceOld[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeByte((byte) (isFavorite ? 1 : 0));
        if (price == 0) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(price);
        }
        dest.writeInt(duration);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SalonServiceOld)) return false;

        SalonServiceOld salonServiceOld = (SalonServiceOld) o;

        if (isFavorite() != salonServiceOld.isFavorite()) return false;
        return getName() != null ? getName().equals(salonServiceOld.getName()) : salonServiceOld.getName() == null;

    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (isFavorite() ? 1 : 0);
        return result;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

}
