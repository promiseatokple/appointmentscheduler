package com.promise.salonapplication.customer.multiselect;

import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

import com.promise.salonapplication.R;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class ServicesGenreAdapter extends ExpandableRecyclerViewAdapter<SalonGenreViewHolder, MultiCheckServicesViewHolder> {


    public ServicesGenreAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public SalonGenreViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public MultiCheckServicesViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
       TextView genrename = parent.findViewById(R.id.list_item_genre_name);
        Log.d("GENRE NAME", genrename.toString());
        return new MultiCheckServicesViewHolder(genrename);
    }

    @Override
    public void onBindChildViewHolder(MultiCheckServicesViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {

    }

    @Override
    public void onBindGroupViewHolder(SalonGenreViewHolder holder, int flatPosition, ExpandableGroup group) {

    }


}
