package com.promise.salonapplication.customer.multiselect;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.promise.salonapplication.R;
import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class SalonMultiCheckGenreAdapter extends
        CheckableChildRecyclerViewAdapter<SalonGenreViewHolder, MultiCheckServicesViewHolder>  {

    public SalonMultiCheckGenreAdapter(List<MultiCheckExpandableGroup> groups) {
        super(groups);
    }
    CheckedTextView checkedTextView;

    TextView tv_price, tv_duration;
    double price;
    int duration;


    @Override
    public MultiCheckServicesViewHolder onCreateCheckChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_multicheck_artist, parent, false);
        checkedTextView = parent.findViewById(R.id.list_item_multicheck_artist_name);
        tv_price  = view.findViewById(R.id.txt_price);
        tv_duration  = view.findViewById(R.id.txt_duration);

        price = Double.valueOf(String.valueOf(tv_price.getText()));
        duration = Integer.valueOf(String.valueOf(tv_duration.getText()));

        return new MultiCheckServicesViewHolder(view);
    }



    @Override
    public void onBindCheckChildViewHolder(MultiCheckServicesViewHolder holder, int position,
                                           CheckedExpandableGroup group, int childIndex) {

//    final SalonServiceOld salonServiceOld = (SalonServiceOld) group.getItems().get(position);
        final SalonServiceOld salonServiceOld = (SalonServiceOld) group.getItems().get(childIndex);
        holder.setServiceName(salonServiceOld.getName());
        holder.setDuration(salonServiceOld.getDuration());
        holder.setPrice(salonServiceOld.getPrice());


    }

    @Override
    public SalonGenreViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_genre, parent, false);

        return new SalonGenreViewHolder(view);
    }

    @Override
    public void onBindGroupViewHolder(SalonGenreViewHolder holder, int flatPosition,
                                      ExpandableGroup group) {
        holder.setGenreTitle(group);

    }

    @Override
    public MultiCheckServicesViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateChildViewHolder(parent, viewType);

    }


}
