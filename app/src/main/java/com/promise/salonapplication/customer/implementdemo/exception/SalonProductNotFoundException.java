package com.promise.salonapplication.customer.implementdemo.exception;

/**
 * Throw this exception to indicate invalid operation on product which does not belong to a shopping cart
 */
public class SalonProductNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 43L;

    private static final String DEFAULT_MESSAGE = "Product is not found in the shopping cart.";

    public SalonProductNotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    public SalonProductNotFoundException(String message) {
        super(message);
    }
}
