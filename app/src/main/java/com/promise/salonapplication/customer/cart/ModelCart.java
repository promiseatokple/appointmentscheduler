package com.promise.salonapplication.customer.cart;

import com.promise.salonapplication.customer.multiselect.SalonServiceOld;

import java.util.ArrayList;

public class ModelCart {
    private ArrayList<SalonServiceOld> cartItems = new ArrayList<SalonServiceOld>();
    public SalonServiceOld getService(int position){
        return cartItems.get(position);
    }
    public void setService(SalonServiceOld salonServiceOld){
        cartItems.add(salonServiceOld);
    }
    public int getCartSize(){

        return cartItems.size();
    }
    public boolean CheckServiceInCart(SalonServiceOld aproduct){
        return cartItems.contains(aproduct);
    }
}

