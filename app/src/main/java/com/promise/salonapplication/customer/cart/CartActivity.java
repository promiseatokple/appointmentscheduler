package com.promise.salonapplication.customer.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.promise.salonapplication.R;
import com.promise.salonapplication.customer.multiselect.SalonServiceOld;
import com.promise.salonapplication.services.SalonApp;


public class CartActivity  extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final LinearLayout layout = (LinearLayout)findViewById(R.id.linearMain);
        final Button btn = (Button)findViewById(R.id.second);
        final SalonApp ct = (SalonApp) getApplicationContext();

        int productsize = ct.getServiceArraylistsize();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        for (int j=0;j< productsize;j++){
            String pName = ct.getServices(j).getName();
            double pPrice = ct.getServices(j).getPrice();
            LinearLayout la = new LinearLayout(this);
            la.setOrientation(LinearLayout.HORIZONTAL);
            TextView tv = new TextView(this);
            tv.setText(" "+pName+" ");
            la.addView(tv);
            TextView tv1 = new TextView(this);
            tv1.setText("$"+pPrice+" ");
            la.addView(tv1);
            final Button btn1 = new Button(this);
            btn1.setId(j+1);
            btn1.setText("Add to Cart");
            btn1.setLayoutParams(params);
            final int index = j;
            btn1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
// TODO Auto-generated method stub
                    Log.i("TAG", "index:"+index);
                    SalonServiceOld productsObject = ct.getServices(index);
                    if(!ct.getCart().CheckServiceInCart(productsObject)){
                        btn1.setText("Item Added");
                        ct.getCart().setServices(productsObject);
                        Toast.makeText(getApplicationContext(), "New CartSize:" +ct.getCart().getCartSize(),Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Products"+(index+1)+"Already Added",Toast.LENGTH_LONG ).show();
                    }
                }
            });
            la.addView(btn1);
            layout.addView(la);
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub
                Intent in = new Intent(getBaseContext(),Screen2.class);
                startActivity(in);
            }
        });
    }
}

