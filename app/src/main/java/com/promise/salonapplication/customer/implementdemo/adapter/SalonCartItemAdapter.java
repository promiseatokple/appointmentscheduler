package com.promise.salonapplication.customer.implementdemo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.promise.salonapplication.R;
import com.promise.salonapplication.customer.implementdemo.SalonConstant;
import com.promise.salonapplication.customer.implementdemo.model.SalonCart;
import com.promise.salonapplication.customer.implementdemo.model.SalonCartItem;
import com.promise.salonapplication.customer.implementdemo.util.SalonCartHelper;
import com.promise.salonapplication.customer.multiselect.MultiCheckServicesViewHolder;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;


public class SalonCartItemAdapter extends BaseAdapter {
    private static final String TAG = "CartItemAdapter";

    private List<SalonCartItem> cartItems = Collections.emptyList();

    private final Context context;
    public static void start(Context context) {
        Intent starter = new Intent(context, SalonCartItemAdapter.class);
        starter.putExtra("", "");
        context.startActivity(starter);
    }

    public SalonCartItemAdapter(Context context) {
        this.context = context;
    }

    public SalonCartItemAdapter(MultiCheckServicesViewHolder multiCheckServicesViewHolder) {

        context = null;
    }

    public void updateCartItems(List<SalonCartItem> cartItems) {
        this.cartItems = cartItems;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return cartItems.size();
    }

    @Override
    public SalonCartItem getItem(int position) {
        return cartItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        TextView tvName;
        TextView tvUnitPrice;
        TextView tvQuantity;
        TextView tvPrice;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.demo_adapter_cart_item, parent, false);
            tvName = (TextView) convertView.findViewById(R.id.tvCartItemName);
            tvUnitPrice = (TextView) convertView.findViewById(R.id.tvCartItemUnitPrice);
            tvQuantity = (TextView) convertView.findViewById(R.id.tvCartItemQuantity);
            tvPrice = (TextView) convertView.findViewById(R.id.tvCartItemPrice);
            convertView.setTag(new ViewHolder(tvName, tvUnitPrice, tvQuantity, tvPrice));
        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            tvName = viewHolder.tvCartItemName;
            tvUnitPrice = viewHolder.tvCartItemUnitPrice;
            tvQuantity = viewHolder.tvCartItemQuantity;
            tvPrice = viewHolder.tvCartItemPrice;
        }

        final SalonCart cart = SalonCartHelper.getCart();
        final SalonCartItem cartItem = getItem(position);
        tvName.setText(cartItem.getProduct().getName());
        tvUnitPrice.setText(SalonConstant.CURRENCY+String.valueOf(cartItem.getProduct().getItemPrice().setScale(2, BigDecimal.ROUND_HALF_UP)));
        tvQuantity.setText(String.valueOf(cartItem.getQuantity()));
        tvPrice.setText(SalonConstant.CURRENCY+String.valueOf(cart.getCost(cartItem.getProduct()).setScale(2, BigDecimal.ROUND_HALF_UP)));
        return convertView;
    }

    private static class ViewHolder {
        public final TextView tvCartItemName;
        public final TextView tvCartItemUnitPrice;
        public final TextView tvCartItemQuantity;
        public final TextView tvCartItemPrice;

        public ViewHolder(TextView tvCartItemName, TextView tvCartItemUnitPrice, TextView tvCartItemQuantity, TextView tvCartItemPrice) {
            this.tvCartItemName = tvCartItemName;
            this.tvCartItemUnitPrice = tvCartItemUnitPrice;
            this.tvCartItemQuantity = tvCartItemQuantity;
            this.tvCartItemPrice = tvCartItemPrice;
        }
    }
}
