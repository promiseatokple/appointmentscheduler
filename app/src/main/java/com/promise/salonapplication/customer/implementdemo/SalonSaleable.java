package com.promise.salonapplication.customer.implementdemo;

import java.math.BigDecimal;

/**
 * Implements this interface for any service which can be added to shopping cart
 */
public interface SalonSaleable {
    BigDecimal getItemPrice();

    String getName();
}
