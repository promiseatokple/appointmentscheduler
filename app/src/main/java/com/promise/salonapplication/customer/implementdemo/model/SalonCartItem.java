package com.promise.salonapplication.customer.implementdemo.model;

public class SalonCartItem {
    private SalonService product;
    private int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public SalonService getProduct() {
        return product;
    }

    public void setProduct(SalonService product) {
        this.product = product;
    }

    public void setServiceNew(SalonService product) {
        this.product = product;
    }

}
