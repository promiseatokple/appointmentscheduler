package com.promise.salonapplication.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.promise.salonapplication.Common.Common;
import com.promise.salonapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerLoginActivity extends AppCompatActivity {

    @BindView(R.id.edt_customer_email)
    EditText edt_customer_email;
    @BindView(R.id.edt_customer_password)
    EditText edt_customer_password;
    @BindView(R.id.btn_customer_login)
    Button btn_customer_login;
    @BindView(R.id.btn_customer_sign_up)
    Button btn_customer_sign_up;


    String user_token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_login);
        ButterKnife.bind(this);
        //Get Firebase auth instance

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TOKEN ERROR: ", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        user_token = task.getResult().getToken();

                        // Log and toast
                        String msg = String.valueOf(user_token);
                        Log.d("TOKEN ", msg);
                        Toast.makeText(CustomerLoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

        if (Common.getCurrentUser() != null) {
            Intent loginIntent = new Intent(CustomerLoginActivity.this, CustomerHomeActivity.class);
            loginIntent.putExtra("user_email_address", Common.getUserEmail());
            startActivity(loginIntent);
            Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show();
            finish();
        }

        btn_customer_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(CustomerLoginActivity.this, CustomerRegisterActivity.class);
                startActivity(registerIntent);
            }
        });


        btn_customer_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edt_customer_email.getText().toString();
                final String password = edt_customer_password.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }



                //authenticate user
                Common.auth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(CustomerLoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.

                                if (!task.isSuccessful()) {
                                    // there was an error
                                    if (password.length() < 6) {
                                        edt_customer_password.setError(getString(R.string.minimum_password));
                                    } else {
                                        Toast.makeText(CustomerLoginActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                    }
                                } else {

                                    Common.customersReference().child(Common.getUserID()).child("deviceToken")
                                            .setValue(user_token)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Intent loginIntent = new Intent(CustomerLoginActivity.this, CustomerHomeActivity.class);
//                                    loginIntent.putExtra("user_email_address", Common.getUserEmail());
                                                    startActivity(loginIntent);
                                                }
                                            });



                                }
                            }
                        }).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {


                    }
                });
            }
        });
    }
}
