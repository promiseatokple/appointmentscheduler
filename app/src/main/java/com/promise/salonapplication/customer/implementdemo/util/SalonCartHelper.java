package com.promise.salonapplication.customer.implementdemo.util;

import com.promise.salonapplication.customer.implementdemo.model.SalonCart;

/**
 * A helper class to retrieve the static shopping cart. Call {@code getCart()} to retrieve the shopping cart before you perform any operation on the shopping cart.
 *
 *
 */
public class SalonCartHelper {
    private static SalonCart cart = new SalonCart();

    /**
     * Retrieve the shopping cart. Call this before perform any manipulation on the shopping cart.
     *
     * @return the shopping cart
     */
    public static SalonCart getCart() {
        if (cart == null) {
            cart = new SalonCart();
        }

        return cart;
    }
}
