package com.promise.salonapplication.customer;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.promise.salonapplication.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerHistoryFragment extends Fragment {


    public CustomerHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_customer_history, container, false);
    }

}
