package com.promise.salonapplication.customer.implementdemo.model;

import com.promise.salonapplication.customer.implementdemo.SalonSaleable;

import java.io.Serializable;
import java.math.BigDecimal;


public class SalonService implements  Serializable, SalonSaleable {
    private int id;
    private String name;
    private double price;
    private int duration;
    public BigDecimal salonPrice;

    public SalonService() {
        super();
    }

    public SalonService(String name, double price, int duration) {
        setId(id);
        setName(name);
        setPrice(price);

        setDuration(duration);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof SalonService)) return false;

        return (this.id == ((SalonService) o).getId());
    }

    public int hashCode() {
        final int prime = 31;
        int hash = 1;
        hash = hash * prime + id;
        hash = hash * prime + (name == null ? 0 : name.hashCode());
        hash = (int) (hash * prime + price);
        hash = hash * prime + (duration);

        return hash;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public BigDecimal getItemPrice() {
        return salonPrice;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    public double getPrice(){
        return this.price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

}
