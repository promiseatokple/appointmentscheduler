package com.promise.salonapplication.customer;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.promise.salonapplication.Common.Common;
import com.promise.salonapplication.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CustomerSettingsActivity extends AppCompatActivity {

    @BindView(R.id.customer_settings_email)
    EditText customer_settings_email;
    @BindView(R.id.customer_settings_full_name)
    EditText customer_settings_full_name;
    @BindView(R.id.customer_settings_phone_number)
    EditText customer_settings_phone_number;

    @BindView(R.id.customer_settings_save_changes_button)
    Button customer_settings_save_changes_button;
    @BindView(R.id.customer_settings_chooseImage_button)
    Button customer_settings_chooseImage_button;

    @BindView(R.id.customer_settings_profile_image)
    CircleImageView customer_settings_profile_image;

    private static final int PICK_IMAGE_REQUEST = 34;
    private Uri filePath;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_settings);

        ButterKnife.bind(this);
auth = FirebaseAuth.getInstance();

        if (auth != null) {
            String currentUserID = auth.getUid();
            if (currentUserID != null) {
                FirebaseDatabase.getInstance().getReference().child("Customers")
                        .child(currentUserID)
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String display_name = String.valueOf(dataSnapshot.child("displayName").getValue());
                                String email_address = String.valueOf(dataSnapshot.child("emailAddress").getValue());
                                String phone_number = String.valueOf(dataSnapshot.child("phoneNumber").getValue());
                                String profile_image = String.valueOf(dataSnapshot.child("profileImage").getValue());


                                customer_settings_full_name.setText(display_name);
                                customer_settings_email.setText(email_address);
                                customer_settings_phone_number.setText(phone_number);
                Picasso.get().load(profile_image).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_user).fit().into(customer_settings_profile_image, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        Picasso.get().load(profile_image).placeholder(R.drawable.default_user).fit().into(customer_settings_profile_image);
                                    }
                                });
                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
            }


        }

        customer_settings_chooseImage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        customer_settings_save_changes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile();



            }
        });
    }

    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }


    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                customer_settings_profile_image.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadFile() {
        //checking if file is available
        Common.auth = FirebaseAuth.getInstance();
        final String STORAGE_PATH_UPLOADS = "Customers/" + Common.getUserID();
        if (filePath != null) {
            //displaying progress dialog while image is uploading
            final ProgressDialog progressDialog = new ProgressDialog(CustomerSettingsActivity.this);
            progressDialog.setTitle("Uploading");
            progressDialog.show();

            //getting the storage reference
            StorageReference sRef = Common.storageReference.child(STORAGE_PATH_UPLOADS + System.currentTimeMillis() + "." + getFileExtension(filePath));

            //adding the file to reference
            sRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //dismissing the progress dialog


                            //displaying success toast
                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();

                            Task<Uri> uriTask = sRef.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    return sRef.getDownloadUrl();
                                }

                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    if(task.isSuccessful())
                                    {
                                        Uri downloadUrl = Uri.parse(String.valueOf(task.getResult()));


                                        HashMap<String, String> custom = new HashMap<>();
                                        custom.put("displayName", customer_settings_full_name.getText().toString());
                                        custom.put("phoneNumber", customer_settings_phone_number.getText().toString());
                                        custom.put("profileImage", downloadUrl.toString());
                                        custom.put("emailAddress", customer_settings_email.getText().toString());

                                        Common.uniqueCustomerReference().addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                dataSnapshot.getRef().setValue(custom);
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });


                                        Log.d("URL", downloadUrl.toString());

                                    }
                                }


                            });
                            progressDialog.dismiss();
                            Intent intent = new Intent(CustomerSettingsActivity.this, CustomerHomeActivity.class);
                            startActivity(intent);

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying the upload progress
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        } else {
            startActivity(new Intent(CustomerSettingsActivity.this, CustomerHomeActivity.class));
        }
    }
}
