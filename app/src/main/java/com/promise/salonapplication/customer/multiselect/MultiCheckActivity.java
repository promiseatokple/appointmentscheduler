package com.promise.salonapplication.customer.multiselect;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.promise.salonapplication.Common.OrderActivity;
import com.promise.salonapplication.R;
import com.promise.salonapplication.customer.implementdemo.SalonProductActivity;
import com.promise.salonapplication.customer.implementdemo.SalonSaleable;
import com.promise.salonapplication.customer.implementdemo.model.SalonCart;
import com.promise.salonapplication.customer.implementdemo.model.SalonCartItem;
import com.promise.salonapplication.customer.implementdemo.model.SalonService;
import com.thoughtbot.expandablecheckrecyclerview.listeners.OnCheckChildClickListener;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.promise.salonapplication.customer.multiselect.SalonGenreDataFactory.makeMultiCheckSalonGenres;


public class MultiCheckActivity extends AppCompatActivity {

    private static SalonMultiCheckGenreAdapter adapter;
    public static TextView cart_count;
    public Button card_continue_to_checkout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_check);

        card_continue_to_checkout = findViewById(R.id.card_continue_to_checkout);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new SalonMultiCheckGenreAdapter(makeMultiCheckSalonGenres());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);


        card_continue_to_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MultiCheckActivity.this, OrderActivity.class));
            }
        });

        adapter.setChildClickListener(new OnCheckChildClickListener() {
            @Override
            public void onCheckChildCLick(View v, boolean checked, CheckedExpandableGroup group, int childIndex) {
                List<SalonServiceOld> serviceList = new ArrayList<>();
                SalonCartItem cartItem = new SalonCartItem();
                SalonService salonService = new SalonService();
                SalonCart cart = new SalonCart();
                if (checked) {



                    if(group.getTitle().equalsIgnoreCase("Hair Extensions")){
                        serviceList.addAll(SalonGenreDataFactory.makeWeaveOnServices());

//                        SalonService product = Constant.PRODUCT_LIST.get(childIndex);
                        SalonServiceOld ser = serviceList.get(childIndex);
                        Intent intent = new Intent(MultiCheckActivity.this, SalonProductActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("service", ser);
                        Log.d("View service Price: ", String.valueOf(ser.getPrice()));
                        intent.putExtras(bundle);
                        //startActivity(intent);


                       /* String name = salonService.getName();
                        cart.getCost(salonService);
*/
                        Log.d("Name: ", String.valueOf(ser.getName()));

                    }
                       else if(group.getTitle().equalsIgnoreCase("Beauty Parlour")){
                           serviceList.addAll(SalonGenreDataFactory.makeBeautyParlourServices());

//                        SalonService product = Constant.PRODUCT_LIST.get(childIndex);
                        SalonServiceOld ser = serviceList.get(childIndex);
                        Intent intent = new Intent(MultiCheckActivity.this, SalonProductActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("service", ser);
                        Log.d("View service Price: ", String.valueOf(ser.getPrice()));
                        intent.putExtras(bundle);
                        //startActivity(intent);

//
//                        String name = salonService.getName();
//                        cart.getCost(salonService);

                        Log.d("Name: ", String.valueOf(ser.getName()));
                    }
                       else if(group.getTitle().equalsIgnoreCase("Washing")){
                           serviceList.addAll(SalonGenreDataFactory.makeWashingDepartmentServices());

//                        SalonService product = Constant.PRODUCT_LIST.get(childIndex);
                        SalonServiceOld ser = serviceList.get(childIndex);
                        Intent intent = new Intent(MultiCheckActivity.this, SalonProductActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("service", ser);
                        Log.d("View service Price: ", String.valueOf(ser.getPrice()));
                        intent.putExtras(bundle);
                        //startActivity(intent);

//
//                        String name = salonService.getName();
//                        cart.getCost(salonService);

                        Log.d("Name: ", String.valueOf(ser.getName()));

                        SalonServiceOld service = new SalonServiceOld(ser.getName(),ser.getPrice(), ser.getDuration());

                    /*    Common.userBooking.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Common.userBooking.setValue(service);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });*/

//                        Common.userBooking.setValue(service).addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                if(task.isSuccessful()){
//                                    Log.d("TASK: ", "SAVED BOOKING");
//                                }
//                            }
//                        });

                    }
                      else if(group.getTitle().equalsIgnoreCase("Styling")){
                           serviceList.addAll(SalonGenreDataFactory.makeStylingDepartmentServices());

//                        SalonService product = Constant.PRODUCT_LIST.get(childIndex);
                        SalonServiceOld ser = serviceList.get(childIndex);
                        Intent intent = new Intent(MultiCheckActivity.this, SalonProductActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("service", ser);
                        Log.d("View service Price: ", String.valueOf(ser.getPrice()));
                        intent.putExtras(bundle);
                        //startActivity(intent);

//
//                        String name = salonService.getName();
//                        cart.getCost(salonService);

                        SalonServiceOld service = new SalonServiceOld(ser.getName(),ser.getPrice(), ser.getDuration());

//                        Common.userBooking.setValue(service).addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                if(task.isSuccessful()){
//                                    Log.d("TASK: ", "SAVED BOOKING");
//                                }
//                            }
//                        });

                        Log.d("Name: ", String.valueOf(ser.getName()));
                    }






                    Log.d("Clicked", "Checked");
/*
                    salonService.setName(group.getItems().get(childIndex).toString());
                    salonService.setPrice(adapter.price);


                    cart.add(salonService, 1);
                    cartItem.setProduct(salonService);

                    getCartItems(cart);*/



                } else
                    Log.d("Clicked", "unchecked");

//                Common.userBooking.addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });

//                cart.remove(cartItem.getProduct());
//                Log.d("Removed Item: ", "") ;

            }


        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        adapter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        adapter.onRestoreInstanceState(savedInstanceState);
    }



    private List<SalonCartItem> getCartItems(SalonCart cart) {
        List<SalonCartItem> cartItems = new ArrayList<>();
        Log.d("TAG", "Current shopping cart: " + cart.getProducts());

        Map<SalonSaleable, Integer> itemMap = cart.getItemWithQuantity();

        for (Map.Entry<SalonSaleable, Integer> entry : itemMap.entrySet()) {
            SalonCartItem cartItem = new SalonCartItem();
            cartItem.setProduct((SalonService) entry.getKey());
            cartItem.setQuantity(entry.getValue());
            cartItems.add(cartItem);
            Log.d("TAG", "Cart item list: " + cart.getProducts());
        }


//        Log.d("TAG", "Cart item list: " + cartItems);
        return cartItems;
    }

}
