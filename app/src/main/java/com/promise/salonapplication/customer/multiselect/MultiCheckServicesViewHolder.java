package com.promise.salonapplication.customer.multiselect;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.promise.salonapplication.R;
import com.promise.salonapplication.customer.implementdemo.SalonSaleable;
import com.promise.salonapplication.customer.implementdemo.adapter.SalonCartItemAdapter;
import com.promise.salonapplication.customer.implementdemo.model.SalonCart;
import com.promise.salonapplication.customer.implementdemo.model.SalonCartItem;
import com.promise.salonapplication.customer.implementdemo.model.SalonService;
import com.promise.salonapplication.customer.implementdemo.util.SalonCartHelper;
import com.promise.salonapplication.services.SalonApp;
import com.thoughtbot.expandablecheckrecyclerview.viewholders.CheckableChildViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MultiCheckServicesViewHolder extends CheckableChildViewHolder implements Checkable {

    private final String TAG = "Customer TAG:";

    final SalonApp ct = new SalonApp();
    LayoutInflater inflater;
    double price;
    int duration;

    final SalonCartItemAdapter cartItemAdapter = new SalonCartItemAdapter(MultiCheckServicesViewHolder.this);


    private CheckedTextView childCheckedTextView;
    private TextView tv_duration, tv_price;
    private TextView cart_count = MultiCheckActivity.cart_count;

    final SalonCart cart = SalonCartHelper.getCart();

    public MultiCheckServicesViewHolder(View itemView) {
        super(itemView);

        childCheckedTextView = (CheckedTextView) itemView.findViewById(R.id.list_item_multicheck_artist_name);

        tv_price  = itemView.findViewById(R.id.txt_price);
        tv_duration  = itemView.findViewById(R.id.txt_duration);
        cartItemAdapter.updateCartItems(getCartItems(cart));


        price = Double.valueOf(String.valueOf(tv_price.getText()));
        duration = Integer.valueOf(String.valueOf(tv_duration.getText()));


    }

    @Override
    public Checkable getCheckable() {

            return childCheckedTextView;
    }

    public void setServiceName(String name) {
        childCheckedTextView.setText(name);
        //Log.d("SERVICE NAME: ", name);
        setChecked(false);
    }

    public void getName(int index){


    }

    public void setPrice(double price){
        setChecked(false);
        this.price = price;
        tv_price.setText( new StringBuilder("GHC").append(price));

       // Log.d("SERVICE PRICE: ", String.valueOf(price));
    }

    public double getPrice() {
        return price;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
        tv_duration.setText(new StringBuilder(String.valueOf(duration)).append(" hours"));
    }

    private List<SalonCartItem> getCartItems(SalonCart cart) {
        List<SalonCartItem> cartItems = new ArrayList<>();
        //Log.d(TAG, "Current shopping cart: " + cart);

        Map<SalonSaleable, Integer> itemMap = cart.getItemWithQuantity();

        for (Map.Entry<SalonSaleable, Integer> entry : itemMap.entrySet()) {
            SalonCartItem cartItem = new SalonCartItem();
            cartItem.setProduct((SalonService) entry.getKey());
            cartItem.setQuantity(entry.getValue());
            cartItems.add(cartItem);
        }

      ct.getCart().getCartSize();

       // Log.d(TAG, "Cart item list: " + cartItems);
        return cartItems;
    }

    @Override
    public void setChecked(boolean checked) {

    }

    @Override
    public boolean isChecked() {
        return false;
    }

    @Override
    public void toggle() {

    }

}
