package com.promise.salonapplication.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import com.promise.salonapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimeSlotActivity extends AppCompatActivity {

    @BindView(R.id.card_go_select_beatician)
    CardView card_go_select_beatician;

    @BindView(R.id.time_picker)
    TimePicker time_picker;

    String time = "";
    String serviceName, price, category_name, selected_services;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slot);

        ButterKnife.bind(this);
        time = String.valueOf(new StringBuilder().append(time_picker.getHour()).append(":").append(time_picker.getMinute()));

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
           selected_services =   bundle.getString("selected_services", "No Service");
           serviceName = bundle.getString("serviceName");
           price = bundle.getString("price");
           category_name = bundle.getString("category_name");

            bundle.putString("selected_services", selected_services);
            bundle.putString("serviceName", serviceName);
            bundle.putString("category_name", category_name);
            bundle.putString("time", time);

            Intent intent = new Intent(TimeSlotActivity.this, ChooseBeauticianActivity.class);
            intent.putExtras(bundle);
        }



        card_go_select_beatician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TimeSlotActivity.this, ChooseBeauticianActivity.class);
                intent.putExtra("time", time);
                Toast.makeText(TimeSlotActivity.this, time, Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });
    }
}
