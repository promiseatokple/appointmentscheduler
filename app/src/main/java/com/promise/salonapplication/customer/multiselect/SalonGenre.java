package com.promise.salonapplication.customer.multiselect;

import android.os.Parcel;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class SalonGenre extends ExpandableGroup<SalonServiceOld> {


    private int iconResId;

    public SalonGenre(String title, List<SalonServiceOld> items , int iconResId) {
        super(title, items);
        this.iconResId = iconResId;
    }

    protected SalonGenre(Parcel in) {
        super(in);
    }

    public int getIconResId() {
        return iconResId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SalonGenre)) return false;

        SalonGenre salonGenre = (SalonGenre) o;

        return getIconResId() == salonGenre.getIconResId();

    }

    @Override
    public int hashCode() {
        return getIconResId();
    }
}

