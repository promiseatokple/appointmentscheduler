package com.promise.salonapplication.customer.implementdemo.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.promise.salonapplication.R;
import com.promise.salonapplication.customer.implementdemo.SalonConstant;
import com.promise.salonapplication.customer.implementdemo.model.SalonService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class SalonProductAdapter extends BaseAdapter {
    private static final String TAG = "ProductAdapter";

    private List<SalonService> products = new ArrayList<>();

    private final Context context;

    public SalonProductAdapter(Context context) {
        this.context = context;
    }

    public void updateProducts(List<SalonService> products) {
        this.products.addAll(products);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public SalonService getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tvName;
        TextView tvPrice;
        TextView tvServiceDuration;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.demo_adapter_product, parent, false);
            tvName = (TextView) convertView.findViewById(R.id.tvProductName);
            tvPrice = (TextView) convertView.findViewById(R.id.tvProductPrice);
            tvServiceDuration = (TextView) convertView.findViewById(R.id.tvServiceDuration);

        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            tvName = viewHolder.tvProductName;
            tvPrice = viewHolder.tvProductPrice;
            tvServiceDuration = viewHolder.tvServiceDuration;
        }

        final SalonService product = getItem(position);
        tvName.setText(product.getName());
        tvPrice.setText(SalonConstant.CURRENCY+String.valueOf(product.getItemPrice().setScale(2, BigDecimal.ROUND_HALF_UP)));
        Log.d(TAG, "Context package name: " + context.getPackageName());
        tvServiceDuration.setText(product.getDuration());

        return convertView;
    }

    private static class ViewHolder {
        public final TextView tvProductName;
        public final TextView tvProductPrice;
        public final TextView tvServiceDuration;

        public ViewHolder(TextView tvProductName, TextView tvProductPrice, TextView tvServiceDuration) {
            this.tvProductName = tvProductName;
            this.tvProductPrice = tvProductPrice;
            this.tvServiceDuration = tvServiceDuration;
        }
    }
}
