package com.promise.salonapplication.customer.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.promise.salonapplication.R;
import com.promise.salonapplication.services.SalonApp;

public class Screen1 extends AppCompatActivity {
    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
// TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen1);
        Button btn3 = findViewById(R.id.third);
        TextView showCartContent = (TextView)findViewById(R.id.showcart);
        TextView price = (TextView)findViewById(R.id.price);
        TextView duration = (TextView)findViewById(R.id.duration);
        final SalonApp ct = (SalonApp) getApplicationContext();
        final int CartSize = ct.getCart().getCartSize();
        String serviceName = "";
        double servicePrice = 9.5;
        int serviceDuration = 0;
        if (CartSize > 0) {
        for(int i=0;i<CartSize;i++){
            String pName = ct.getCart().getServices(i).getName();
            double  pPrice = ct.getCart().getServices(i).getPrice();
            int pDisc = ct.getCart().getServices(i).getDuration();
            serviceName = "Product Name:"+pName ;
            serviceDuration = pDisc;
            servicePrice = pPrice;
        }
        } else
            serviceName = "There is no Items in Shopping Cart";
        showCartContent.setText(serviceName);
        btn3.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
// TODO Auto-generated method stub
                                        if (CartSize > 0) {
                                            Intent i = new Intent(getBaseContext(), Screen2.class);
                                            startActivity(i);
                                        } else {

                                            Toast.makeText(getApplicationContext(), "Shopping Cart is Empty", Toast.LENGTH_LONG).show();
                                        }
                                    }

                /* (non-Javadoc)
                 * @see android.app.Activity#onDestroy() */

            });
    }

    @Override
    protected void onDestroy () {
// TODO Auto-generated method stub
        super.onDestroy();
    }
}