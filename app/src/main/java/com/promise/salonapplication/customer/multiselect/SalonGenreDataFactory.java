package com.promise.salonapplication.customer.multiselect;

import android.os.Parcelable;

import com.promise.salonapplication.R;
import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.Arrays;
import java.util.List;

public class SalonGenreDataFactory {

    public static List<ExpandableGroup<? extends Parcelable>> makeGenres() {
        return Arrays.asList(
                makeWeaveOnGenre(),
                makeBeautyParlourGenre(),
                makeWashingDepartmentGenre(),
                makeStylingDepartmentGenre());

    }

    public static List<MultiCheckExpandableGroup> makeMultiCheckSalonGenres() {
        return Arrays.asList(

                makeMultiCheckWeaveOnGenre(),
                makeMultiCheckBeautyParlourGenre(),
                makeMultiCheckClassicGenre(),
                makeMultiCheckStylingDepartmentGenre()
               );
    }



    public static SalonGenre makeWeaveOnGenre() {
        return new SalonGenre("Hair Extensions", makeWeaveOnServices(), R.drawable.hair);
    }

    public static SalonMultiCheckGenre makeMultiCheckWeaveOnGenre() {
        return new SalonMultiCheckGenre("Hair Extensions", makeWeaveOnServices(), R.drawable.ic_electric_guitar);
    }

    public static SingleCheckGenre makeSingleCheckWeaveOnGenre() {
        return new SingleCheckGenre("Hair Extensions", makeWeaveOnServices(), R.drawable.ic_electric_guitar);
    }

    public static List<SalonServiceOld> makeWeaveOnServices() {

        SalonServiceOld microStrands = new SalonServiceOld("Micro-Strands",true,Long.valueOf("4534"),3);
        SalonServiceOld weaveOn = new SalonServiceOld("Weave On",true,Long.valueOf("34"),3);
        SalonServiceOld wigCap = new SalonServiceOld("Wig Cap",true,Long.valueOf("34"),3);
        SalonServiceOld weaveOnWithCutting = new SalonServiceOld("Weave on with cutting and Styling",true,Long.valueOf("34"),3);
        SalonServiceOld frontalWeaveOn = new SalonServiceOld("Frontal Weave on",true,Long.valueOf("34"),3);
        SalonServiceOld wigCapWithCutting = new SalonServiceOld("Wig Cap with cutting",true,Long.valueOf("34"),3);


        return Arrays.asList(microStrands, weaveOn, wigCap, weaveOnWithCutting, frontalWeaveOn, wigCapWithCutting);
//        return Arrays.asList(queen, styx, reoSpeedwagon, boston);
    }


    public static SalonGenre makeBeautyParlourGenre() {
        return new SalonGenre("Beauty Parlour", makeBeautyParlourServices(), R.drawable.nail_polish);
    }

    public static SalonMultiCheckGenre makeMultiCheckBeautyParlourGenre() {
        return new SalonMultiCheckGenre("Beauty Parlour", makeBeautyParlourServices(), R.drawable.nail_polish);
    }

    public static SingleCheckGenre makeSingleCheckBeautyParlourGenre() {
        return new SingleCheckGenre("Beauty Parlour", makeBeautyParlourServices(), R.drawable.ic_saxaphone);
    }

    public static List<SalonServiceOld> makeBeautyParlourServices() {

        SalonServiceOld normalLashes = new SalonServiceOld("Normal Lashes",true,Long.valueOf("34"),3);
        SalonServiceOld minkLashes = new SalonServiceOld("Mink Lashes",true,Long.valueOf("34"),3);
        SalonServiceOld eyebrow = new SalonServiceOld("Eyebrow",true,Long.valueOf("34"),3);
        SalonServiceOld Tinting = new SalonServiceOld("Tinting",true,Long.valueOf("34"),3);
        SalonServiceOld bridalMakeUp = new SalonServiceOld("Bridal Make-Up",true,Long.valueOf("34"),3);

/*
        Artist milesDavis = new Artist("Miles Davis", true);
        Artist ellaFitzgerald = new Artist("Ella Fitzgerald", true);
        Artist billieHoliday = new Artist("Billie Holiday", false);*/

        return Arrays.asList(normalLashes, minkLashes, eyebrow,Tinting,bridalMakeUp );
//        return Arrays.asList(milesDavis, ellaFitzgerald, billieHoliday);
    }

    public static SalonGenre makeWashingDepartmentGenre() {
        return new SalonGenre("Washing", makeWashingDepartmentServices(), R.drawable.eyeshadow);
    }

    public static SalonMultiCheckGenre makeMultiCheckClassicGenre() {
        return new SalonMultiCheckGenre("Washing", makeWashingDepartmentServices(), R.drawable.eyeshadow);
    }

    public static SingleCheckGenre makeSingleCheckWashingDepartmentGenre() {
        return new SingleCheckGenre("Washing", makeWashingDepartmentServices(), R.drawable.ic_violin);
    }

    public static List<SalonServiceOld> makeWashingDepartmentServices() {

        SalonServiceOld washing_and_straightening = new SalonServiceOld("Washing and straightening",true,Long.valueOf("34"),3);
        SalonServiceOld touch_up_and_setting = new SalonServiceOld("Touch up and setting",true,Long.valueOf("34"),3);
        SalonServiceOld touch_up_and_straightening = new SalonServiceOld("Touch up and straightening",true,Long.valueOf("34"),3);
        SalonServiceOld leisure_curls = new SalonServiceOld("Leisure curls",true,Long.valueOf("34"),3);
        SalonServiceOld washing_and_setting = new SalonServiceOld("Washing and setting",true,Long.valueOf("34"),3);


        return Arrays.asList(washing_and_setting, leisure_curls, touch_up_and_straightening, touch_up_and_setting,washing_and_straightening );
    }

    public static SalonGenre makeStylingDepartmentGenre() {
        return new SalonGenre("Styling", makeStylingDepartmentServices(), R.drawable.makeup_brushes);
    }

    public static SalonMultiCheckGenre makeMultiCheckStylingDepartmentGenre() {
        return new SalonMultiCheckGenre("Styling", makeStylingDepartmentServices(), R.drawable.makeup_brushes);
    }

    public static SingleCheckGenre makeSingleCheckStylingDepartmentGenre() {
        return new SingleCheckGenre("Styling", makeStylingDepartmentServices(), R.drawable.makeup_brushes);
    }

    public static List<SalonServiceOld> makeStylingDepartmentServices() {

        SalonServiceOld nor = new SalonServiceOld("Washing and straightening",true,Long.valueOf("34"),3);
        SalonServiceOld mink = new SalonServiceOld("Touch up and setting",true,Long.valueOf("34"),3);
        SalonServiceOld eye = new SalonServiceOld("Touch up and straightening",true,Long.valueOf("34"),3);
        SalonServiceOld Tint = new SalonServiceOld("Leisure curls",true,Long.valueOf("34"),3);
        SalonServiceOld bridal = new SalonServiceOld("Washing and setting",true,Long.valueOf("34"),3);


        return Arrays.asList(nor, mink, eye, Tint,bridal );
    }



}
