package com.promise.salonapplication.customer.cart;

import com.promise.salonapplication.customer.multiselect.SalonServiceOld;

import java.util.ArrayList;

public class SalonModelCart {
    private ArrayList<SalonServiceOld> cartItems = new ArrayList<>();
    public SalonServiceOld getServices(int position){
        return cartItems.get(position);
    }
    public void setServices(SalonServiceOld service){
        cartItems.add(service);
    }
    public int getCartSize(){

        return cartItems.size();
    }
    public boolean CheckServiceInCart(SalonServiceOld serviceModel){
        return cartItems.contains(serviceModel);
    }
}

