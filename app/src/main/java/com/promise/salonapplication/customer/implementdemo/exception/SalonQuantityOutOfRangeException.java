package com.promise.salonapplication.customer.implementdemo.exception;

/**
 * Throw this exception to indicate invalid quantity to be used on a shopping cart product.
 */
public class SalonQuantityOutOfRangeException extends RuntimeException {
    private static final long serialVersionUID = 44L;

    private static final String DEFAULT_MESSAGE = "Quantity is out of range";

    public SalonQuantityOutOfRangeException() {
        super(DEFAULT_MESSAGE);
    }

    public SalonQuantityOutOfRangeException(String message) {
        super(message);
    }
}
