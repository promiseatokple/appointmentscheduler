package com.promise.salonapplication.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.promise.salonapplication.Adapters.CustomerViewPagerAdapter;
import com.promise.salonapplication.Common.Common;
import com.promise.salonapplication.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class CustomerHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;

    @BindView(R.id.customer_home_display_name)
    TextView customer_home_display_name;
    @BindView(R.id.customer_home_phone_number)
    TextView customer_home_phone_number;
    @BindView(R.id.customer_home_email_address)
    TextView customer_home_email_address;
    @BindView(R.id.customer_home_profile_image)
    CircleImageView customer_home_profile_image;

    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_home);
        ButterKnife.bind(this);


        auth = FirebaseAuth.getInstance();

//Common.uniqueCustomerReference().addValueEventListener(new ValueEventListener() {
//        @Override
//        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//            String display_name = String.valueOf(dataSnapshot.child("displayName").getValue());
//            String email_address = String.valueOf(dataSnapshot.child("emailAddress").getValue());
//            String phone_number = String.valueOf(dataSnapshot.child("phoneNumber").getValue());
//            String profile_image = String.valueOf(dataSnapshot.child("profileImage").getValue());
//
//
//            customer_home_display_name.setText(display_name);
//            customer_home_email_address.setText(email_address);
//            customer_home_phone_number.setText(phone_number);
//            Picasso.get().load(profile_image).fit().into(customer_home_profile_image);
//        }
//
//        @Override
//        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//        }
//    });

        if (auth != null) {
            String currentUserID = auth.getUid();
            if (currentUserID != null) {
                FirebaseDatabase.getInstance().getReference().child("Customers")
                        .child(currentUserID)
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String display_name = String.valueOf(dataSnapshot.child("displayName").getValue());
                                String email_address = String.valueOf(dataSnapshot.child("emailAddress").getValue());
                                String phone_number = String.valueOf(dataSnapshot.child("phoneNumber").getValue());
                                String profile_image = String.valueOf(dataSnapshot.child("profileImage").getValue());


                                customer_home_display_name.setText(display_name);
                                customer_home_email_address.setText(email_address);
                                customer_home_phone_number.setText(phone_number);



                                Picasso.get().load(profile_image).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_user).fit().into(customer_home_profile_image, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        Picasso.get().load(profile_image).placeholder(R.drawable.default_user).fit().into(customer_home_profile_image);
                                    }
                                });
                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
            }


        }





        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String emailAddressFromIntent = bundle.getString("user_email_address");
            Toast.makeText(this, emailAddressFromIntent, Toast.LENGTH_LONG).show();
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        tabLayout = findViewById(R.id.tab_layout_id);
        tabLayout.setFitsSystemWindows(true);
        appBarLayout = findViewById(R.id.appbarid);
        appBarLayout.setFitsSystemWindows(true);

        viewPager = findViewById(R.id.viewpager_id);


        CustomerViewPagerAdapter viewPagerAdapter = new CustomerViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(new CustomerHomeFragment(), "HOME");
        viewPagerAdapter.addFragment(new CustomerServicesFragment(), "SERVICES");
        viewPagerAdapter.addFragment(new CustomerHistoryFragment(), "HISTORY");

        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.main_menu_logout) {
            Common.auth.signOut();
            startActivity(new Intent(CustomerHomeActivity.this, CustomerLoginActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_profile) {
//            startActivity(new Intent(CustomerHomeActivity.this, ProfileActivity.class));

        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(CustomerHomeActivity.this, CustomerSettingsActivity.class));

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_emergency) {
            startActivity(new Intent(CustomerHomeActivity.this, CustomerReviews.class));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();

    }
}