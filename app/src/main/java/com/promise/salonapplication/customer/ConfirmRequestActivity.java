package com.promise.salonapplication.customer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.promise.salonapplication.Common.Common;
import com.promise.salonapplication.R;
import com.promise.salonapplication.services.NotificationDetailsActivity;
import com.promise.salonapplication.services.NotificationReceiver;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfirmRequestActivity extends AppCompatActivity {


    private static final String CHANNEL_ID = "Requests";
    private NotificationManagerCompat managerCompat;
    Bundle bundle;

    //region BindViews
    @BindView(R.id.customer_confirm_request_time)
    TextView customer_confirm_request_time;

    @BindView(R.id.customer_confirm_request_beautician_name)
    TextView customer_confirm_request_beautician_name;

    @BindView(R.id.customer_confirm_request_category)
    TextView customer_confirm_request_category;

    @BindView(R.id.customer_confirm_request_service)
    TextView customer_confirm_request_service;

    @BindView(R.id.customer_confirm_service_price)
    TextView customer_confirm_service_price;

//    @BindView(R.id.customer_confirm_service_duration)
//    TextView customer_confirm_service_duration;

    @BindView(R.id.customer_confirm_request_beautician_salon)
    TextView customer_confirm_request_beautician_salon;

    @BindView(R.id.customer_confirm_request_beautician_phone_number)
    TextView customer_confirm_request_beautician_phone_number;

    @BindView(R.id.customer_confirm_request_confirm_button)
    Button customer_confirm_request_confirm_button;
//endregion

    AlertDialog.Builder dialog;

    private static String current_state = "not_accepted";
    private FirebaseAuth auth;

    String customer_display_name, customer_phone_number,
            customer_email_address, customer_profile_image;

    String beautician_name, category_name, duration, service_name, service_price, salon_name, beautician_number, beautician_id, selected_services, time;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_request);
        Common.friendsDatabase().keepSynced(true);
        Common.bookingsDatabase().keepSynced(true);
        Common.uniqueCustomerReference().keepSynced(true);
        Common.uniqueBeauticianReference().keepSynced(true);
        Common.beauticiansReference().keepSynced(true);
        Common.customersReference().keepSynced(true);
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();





        createNotificationChannel();

//        createNotificationChannel();
        if (auth != null) {
            String currentUserID = auth.getUid();
            if (currentUserID != null) {
                FirebaseDatabase.getInstance().getReference().child("Customers")
                        .child(currentUserID)
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                customer_display_name = String.valueOf(dataSnapshot.child("displayName").getValue());
                                customer_email_address = String.valueOf(dataSnapshot.child("emailAddress").getValue());
                                customer_phone_number = String.valueOf(dataSnapshot.child("phoneNumber").getValue());
                                customer_profile_image = String.valueOf(dataSnapshot.child("profileImage").getValue());

                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
            }


        }




         bundle = getIntent().getExtras();

        if (bundle != null) {
             beautician_name = bundle.getString("beautician_name");
             category_name = bundle.getString("category_name");
             duration = bundle.getString("duration");
             service_name = bundle.getString("serviceName");
             service_price = bundle.getString("priceInSummary");
             salon_name = bundle.getString("nameOfSalon");
             beautician_number = bundle.getString("beautician_number");
            beautician_id = bundle.getString("beautician_id");
            selected_services = bundle.getString("selected_services");
            time = bundle.getString("time");


//            customer_confirm_request_time.setText(LocalDateTime.now().toLocalTime().toString());
            customer_confirm_request_time.setText(time);
            customer_confirm_request_beautician_name.setText(beautician_name);
            customer_confirm_service_price.setText(service_price);
//            customer_confirm_request_service.setText(serviceName);
            customer_confirm_request_service.setText(String.valueOf(new StringBuilder().append(selected_services).append(",")));
//            customer_confirm_service_duration.setText(duration);
            customer_confirm_request_category.setText(category_name);
            customer_confirm_request_beautician_salon.setText(salon_name);
            customer_confirm_request_beautician_phone_number.setText(beautician_number);
        }

        dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Confirm Booking").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // TODO: 7/1/19 get the proper position of getSentRequests
                getSentRequests(beautician_id);
                if (current_state.equals("not_accepted")) {
                    sendRequest(beautician_id);



                    //sendNotification();
                }

                if (current_state.equals("req_sent")) {
                    cancelRequest(beautician_id);
                }

//                if (current_state.equals("req_received")) {
//                    requestState(beautician_id);
//                }


//sendNotification();
                Intent intent = new Intent(ConfirmRequestActivity.this, CustomerSettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(ConfirmRequestActivity.this, 0, intent, 0);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(ConfirmRequestActivity.this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle("Your request has been sent")
                        .setContentText("Booking sent successfully. Wait for a confirmation")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Set the intent that will fire when the user taps the notification
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true);


                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(ConfirmRequestActivity.this);

// notificationId is a unique int for each notification that you must define
                int notificationId = 34;
                notificationManager.notify(notificationId, builder.build());
                startActivity(new Intent(ConfirmRequestActivity.this, CustomerHomeActivity.class));

                Log.d("Positive :", "Ok clicked");
            }
        }).setNegativeButton("Cancel Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("Negative :", "Cancel clicked");
                cancelRequest(beautician_id);
                startActivity(new Intent(ConfirmRequestActivity.this, CustomerHomeActivity.class));

            }
        }).setMessage("You have chosen " + new StringBuilder().append(beautician_name.toUpperCase()) + new StringBuilder(" ").append(" for your appointment"));

        dialog.create();
        dialog.show();

        customer_confirm_request_confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

          /*      // TODO: 7/1/19 get the proper position of getSentRequests
                getSentRequests(beautician_id);
                if (current_state.equals("not_accepted")) {
                    sendRequest(beautician_id);



                    //sendNotification();
                }

                if (current_state.equals("req_sent")) {
                    cancelRequest(beautician_id);
                }

//                if (current_state.equals("req_received")) {
//                    requestState(beautician_id);
//                }


//sendNotification();
                Intent intent = new Intent(ConfirmRequestActivity.this, CustomerSettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(ConfirmRequestActivity.this, 0, intent, 0);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(ConfirmRequestActivity.this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle("Your request has been sent")
                        .setContentText("Booking sent successfully. Wait for a confirmation")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Set the intent that will fire when the user taps the notification
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true);


                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(ConfirmRequestActivity.this);

// notificationId is a unique int for each notification that you must define
                int notificationId = 34;
                notificationManager.notify(notificationId, builder.build());*/


            }
        });

    }

    public void sendRequest(String beautician_id) {
        Common.bookingsDatabase().child(Common.getUserID()).child(beautician_id)
                .child("request_type")
                .setValue("sent")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Common.bookingsDatabase().child(beautician_id).child(Common.getUserID())
                                    .child("request_type").setValue("received")
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            HashMap<String, String> notificationData = new HashMap<>();
                                            notificationData.put("display_name", customer_display_name);
                                            notificationData.put("phone_number", customer_phone_number);
                                            notificationData.put("email_address", customer_email_address);
                                            notificationData.put("profile_image", customer_profile_image);
                                            notificationData.put("customer_id",Common.getUserID());
                                            notificationData.put("service", service_name);
                                            notificationData.put("price",service_price);
                                            notificationData.put("duration",duration);
                                            notificationData.put("type", "request");

                                            Common.notificationDatabase().child(beautician_id).push()
                                                    .setValue(notificationData)
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {

                                                }
                                            });

                                                    current_state = "req_sent";
                                            customer_confirm_request_confirm_button.setText(R.string.oops);
                                            Toast.makeText(ConfirmRequestActivity.this,
                                                    "Success", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                        } else {
                            Toast.makeText(ConfirmRequestActivity.this,
                                    "Request sending failed", Toast.LENGTH_SHORT).show();

                        }


                    }
                });

    }

    public void cancelRequest(String beautician_id) {
        if (current_state.equals("req_sent")) {
            Common.bookingsDatabase().child(Common.getUserID()).child(beautician_id)
                    .removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Common.bookingsDatabase().child(beautician_id).child(Common.getUserID())
                            .removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            startActivity(new Intent(ConfirmRequestActivity.this, CustomerHomeActivity.class));
                            finish();
                        }
                    });
                }
            });


        }
    }

    public static void getSentRequests(String beautician_id) {
        Common.bookingsDatabase().child(Common.getUserID())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(beautician_id)) {
                            String request_type = String.valueOf(dataSnapshot.child(beautician_id)
                                    .child("request_type").getValue());

                            if (request_type.equals("received")) {
                                current_state = "req_received";
                                // TODO: 7/1/19 If the other user has this, what is he to do

                                //Button was Accept request

                            } else if (request_type.equals("sent")) {
                                //Button becomes cancel request
                            } else {
                                Common.friendsDatabase().child(Common.getUserID()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.hasChild(beautician_id)) {
                                            current_state = "friends";
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    public static void requestState(String beautician_id) { // implemented as friends list
            Common.friendsDatabase().child(Common.getUserID()).child(beautician_id)
                    .setValue(Common.currentDate).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Common.friendsDatabase().child(beautician_id).child(Common.getUserID()).setValue(Common.currentDate)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                }
                            });
                }
            });

    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    public void  sendNotification(){

        Intent nextActivity = new Intent(ConfirmRequestActivity.this, NotificationDetailsActivity.class);
        PendingIntent pendingIntent = PendingIntent
                .getActivity(ConfirmRequestActivity.this, 0,nextActivity,0);

        Intent broadcastIntent = new Intent(ConfirmRequestActivity.this, NotificationReceiver.class);
        broadcastIntent.putExtra("message", "This is the broadcast message to be be sent along");
        broadcastIntent.putExtra("customer", "This is the name of the customer");
        broadcastIntent.putExtra("service", "The customer has requested for this service");
        broadcastIntent.putExtra("time", "This is the time the customer want to receive the service");

        PendingIntent actionIntent = PendingIntent
                .getBroadcast(ConfirmRequestActivity.this,0, broadcastIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        String CHANNEL_ONE = "News";
        Notification notification = new NotificationCompat.Builder(ConfirmRequestActivity.this, CHANNEL_ONE)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setContentTitle("New Booking")
                .setContentText("Client A has sent you a request")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .addAction(R.mipmap.ic_launcher, "Toast", actionIntent)
                .build();

        managerCompat.notify(1, notification);
    }




    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.CHANNEL_NEWS);
            String description = getString(R.string.CHANNEL_DESCRIPTION);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


}
