package com.promise.salonapplication.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;


import com.promise.salonapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceCategoryActivity extends AppCompatActivity {


    @BindView(R.id.hairs_card)
    CardView hairs_card;

    @BindView(R.id.skin_card)
    CardView skin_card;

    @BindView(R.id.nails_card)
    CardView nails_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_category_activity);

        ButterKnife.bind(this);

        nails_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServiceCategoryActivity.this, NailsActivity.class);
                intent.putExtra("category_name", "Nails");
                startActivity(intent);
            }
        });

        hairs_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServiceCategoryActivity.this, HairsActivity.class);
                intent.putExtra("category_name", "Hair Styles");
                startActivity(intent);
            }
        });

        skin_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServiceCategoryActivity.this, SkinCareActivity.class);
                intent.putExtra("category_name", "Skin Care");
                startActivity(intent);
            }
        });

    }





}