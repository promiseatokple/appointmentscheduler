package com.promise.salonapplication.customer.cart;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.promise.salonapplication.R;
import com.promise.salonapplication.services.SalonApp;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class Screen2 extends AppCompatActivity {
    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle) */

    Adapter adapter;
    RecyclerView cart_recycler;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
// TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen2);
        adapter = new ArrayAdapter<String>(this,R.layout.single_cart);

        cart_recycler = findViewById(R.id.cart_recycler);

        TextView showCartContent = (TextView)findViewById(R.id.showcart);
        TextView price = (TextView)findViewById(R.id.price);
        TextView duration = (TextView)findViewById(R.id.duration);
        final SalonApp ct = (SalonApp) getApplicationContext();
        final int CartSize = ct.getCart().getCartSize();
        String serviceName = "";
        int servicePrice = 0;
        int serviceDuration = 0;
        for(int i=0;i<CartSize;i++){
            String pName = ct.getCart().getServices(i).getName();
            double  pPrice = ct.getCart().getServices(i).getPrice();
            int pDisc = ct.getCart().getServices(i).getDuration();
            serviceName += "Product Name:"+pName ;
            serviceDuration += pDisc;
            servicePrice += pPrice;
        }
        showCartContent.setText(serviceName);
        price.setText(String.valueOf(servicePrice));
        duration.setText(String.valueOf(serviceDuration));
    }


    public class BeauticiansHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_beautician_name)
        TextView tv_beautician_name;
        @BindView(R.id.imageView)
        CircleImageView imageView;
        @BindView(R.id.nameOfSalon)
        TextView tv_name_of_salon;
        @BindView(R.id.rb_beautician_rating)
        RatingBar rb_beautician_rating;



        public BeauticiansHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }


}