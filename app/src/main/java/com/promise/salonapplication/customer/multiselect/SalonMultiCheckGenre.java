package com.promise.salonapplication.customer.multiselect;

import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup;

import java.util.List;

public class SalonMultiCheckGenre extends MultiCheckExpandableGroup {
    private int iconResId;
    public SalonMultiCheckGenre(String title, List<SalonServiceOld> items, int iconResId) {
        super(title, items);
        this.iconResId = iconResId;
    }


    public int getIconResId() {
        return iconResId;
    }
}
