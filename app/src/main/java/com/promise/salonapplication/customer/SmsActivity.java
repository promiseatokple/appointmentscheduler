package com.promise.salonapplication.customer;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.africastalking.AfricasTalking;
import com.africastalking.models.sms.Recipient;
import com.africastalking.services.SmsService;
import com.promise.salonapplication.R;

import java.util.List;

import timber.log.Timber;

public class SmsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        @SuppressLint("StaticFieldLeak") AsyncTask<Void, String, Void> task = new AsyncTask<Void, String, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    Timber.i("Getting sms service");
                    SmsService sms = AfricasTalking.getSmsService();

                    Timber.i("Sending hello to 0718769882");
                    List<Recipient> res = sms.send("hello", new String[]{"0718769882"});

                    sms.send("Paps", "", new String[]{"0246521771"});

                    Timber.i(res.get(0).messageId);
                    Timber.i(res.get(0).status);

                } catch (Exception e) {
                    Timber.e(e, "IOException");
                }

                return null;
            }
        };

        task.execute();

    }
}