package com.promise.salonapplication.Interfaces;

public interface ItemCountListener {
    void itemCount(int count);
}
