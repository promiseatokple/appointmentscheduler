
package com.promise.salonapplication.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.promise.salonapplication.R;

import butterknife.ButterKnife;


public class NotificationActivity extends AppCompatActivity {


Button btn_send_notification;
    private NotificationManagerCompat managerCompat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ButterKnife.bind(this);

        managerCompat = NotificationManagerCompat.from(this);

        btn_send_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent nextActivity = new Intent(NotificationActivity.this, NotificationDetailsActivity.class);
                PendingIntent pendingIntent = PendingIntent
                        .getActivity(NotificationActivity.this, 0,nextActivity,0);

                Intent broadcastIntent = new Intent(NotificationActivity.this, NotificationReceiver.class);
                    broadcastIntent.putExtra("message", "This is the broadcast message to be be sent along");
                    broadcastIntent.putExtra("customer", "This is the name of the customer");
                    broadcastIntent.putExtra("service", "The customer has requested for this service");
                    broadcastIntent.putExtra("time", "This is the time the customer want to receive the service");

                    PendingIntent actionIntent = PendingIntent
                            .getBroadcast(NotificationActivity.this,0, broadcastIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                String CHANNEL_ONE = "Appointments";
                Notification notification = new NotificationCompat.Builder(NotificationActivity.this, CHANNEL_ONE)
                        .setAutoCancel(true)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pendingIntent)
                        .setContentTitle("New Booking")
                        .setContentText("Client A has sent you a request")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                        .addAction(R.mipmap.ic_launcher, "Toast", actionIntent)
                        .build();

                managerCompat.notify(1, notification);
            }
        });

    }

    public void createNotificationChannel(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(String.valueOf(R.string.NEWS_CHANNEL_ID),String.valueOf(R.string.CHANNEL_NEWS), NotificationManager.IMPORTANCE_DEFAULT );
            notificationChannel.setDescription(String.valueOf(R.string.CHANNEL_DESCRIPTION));
            notificationChannel.setShowBadge(true);


            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    public void triggerNotification(){
        Intent intent = new Intent(this, NotificationDetailsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, getString(R.string.NEWS_CHANNEL_ID))
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)

                .setContentTitle("Notification Title")
                .setContentText("This is text, that will be shown as part of notification")
                .setStyle(new NotificationCompat.BigTextStyle().bigText("This is text, that will be shown as part of notification"))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setChannelId(getString(R.string.NEWS_CHANNEL_ID))
                .setAutoCancel(true);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(getResources().getInteger(R.integer.notificationId), builder.build());


    }


    //  .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.ic_icon_large))
}

