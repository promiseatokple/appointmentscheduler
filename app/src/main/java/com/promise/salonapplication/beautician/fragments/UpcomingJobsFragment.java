package com.promise.salonapplication.beautician.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.promise.salonapplication.R;


public class UpcomingJobsFragment extends Fragment {
    View view;

    public UpcomingJobsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.history_upcoming_fragment, container, false);
//
//        view =  super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }
}
