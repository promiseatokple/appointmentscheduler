package com.promise.salonapplication.beautician.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.promise.salonapplication.Common.Common;
import com.promise.salonapplication.Common.Request;
import com.promise.salonapplication.R;
import com.promise.salonapplication.services.MyAppsNotificationManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;

public class BeauticianHomeFragment extends Fragment  {
View view;
@BindView(R.id.beautician_orders)
        RecyclerView beautician_orders;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

AlertDialog alertDialog;

ListenerRegistration listenerRegistration;
ListenerRegistration bookingRealtimelistener;

    private FirebaseFirestore db;
    private FirebaseRecyclerAdapter<Request, BeauticiansHomeHolder> adapter;
    LinearLayoutManager linearLayoutManager;
    MyAppsNotificationManager myAppsNotificationManager;
    private FirebaseDatabase database;

//INotificationCountListener iNotificationCountListener;


    @Nullable
    @Override
    public Context getContext() {
        return super.getContext();
    }

    public BeauticianHomeFragment() {

    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_beautician_home, container, false);
        database = FirebaseDatabase.getInstance();

        beautician_orders = view.findViewById(R.id.beautician_orders);

        alertDialog = new  SpotsDialog.Builder().setContext(getActivity()).setMessage("Checking for new Requests").setCancelable(true).setCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        }).build();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

        init();
        getRequests();



        view =  super.onCreateView(inflater, container, savedInstanceState);
        return view;

    }



    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {
        if(listenerRegistration != null){
            listenerRegistration.remove();
        }
        if(bookingRealtimelistener != null){
            bookingRealtimelistener.remove();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if(listenerRegistration != null){
            listenerRegistration.remove();
        }
        if(bookingRealtimelistener != null){
            bookingRealtimelistener.remove();
        }
        super.onDestroy();
    }


    private void init(){
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        beautician_orders.setLayoutManager(linearLayoutManager);
        db = FirebaseFirestore.getInstance();
    }

    private void getRequests(){

        Query query1  = database.getReference().child("Beauticians").child(Common.getUserID()).child("Requests");


        FirebaseRecyclerOptions<Request> firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Request>()
                .setQuery(query1, Request.class).build();

        adapter = new FirebaseRecyclerAdapter<Request, BeauticianHomeFragment.BeauticiansHomeHolder>(firebaseRecyclerOptions) {
            @Override
            public void onBindViewHolder(@NonNull BeauticianHomeFragment.BeauticiansHomeHolder holder, int position, @NonNull Request model) {
                //progressBar.setVisibility(View.GONE);
                alertDialog.dismiss();

//
                holder.who_sent_the_request.setText(String.valueOf(model.getCustomerName()));

                holder.beautician_home_accept_offer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                        databaseReference.child("Beauticians")
                                .child(Common.getUserID()).child("Requests").setValue(new Request());
                    }
                });










            }

            @NonNull
            @Override
            public BeauticianHomeFragment.BeauticiansHomeHolder onCreateViewHolder(@NonNull ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.single_request, group, false);




                return new BeauticianHomeFragment.BeauticiansHomeHolder(view);
            }

        };

        adapter.notifyDataSetChanged();
        beautician_orders.setAdapter(adapter);



    }




    public class BeauticiansHomeHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.who_sent_the_request)
        TextView who_sent_the_request;
        @BindView(R.id.beautician_home_decline_offer)
        CardView beautician_home_decline_offer;

        @BindView(R.id.beautician_home_accept_offer)
        CardView beautician_home_accept_offer;



        public BeauticiansHomeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}


