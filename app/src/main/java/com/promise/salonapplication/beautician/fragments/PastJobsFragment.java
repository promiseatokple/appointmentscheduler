package com.promise.salonapplication.beautician.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.promise.salonapplication.R;


public class PastJobsFragment extends Fragment {
    View view;

    public PastJobsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.history_past_fragment, container, false);

//        view =  super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }
}
