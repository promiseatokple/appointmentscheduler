package com.promise.salonapplication.beautician;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.promise.salonapplication.Adapters.CustomerViewPagerAdapter;
import com.promise.salonapplication.R;
import com.promise.salonapplication.beautician.fragments.BeauticianHomeFragment;
import com.promise.salonapplication.beautician.fragments.PastJobsFragment;
import com.promise.salonapplication.beautician.fragments.PendingJobsFragment;
import com.promise.salonapplication.beautician.fragments.UpcomingJobsFragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class BeauticianHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;


    @BindView(R.id.beautician_home_display_name)
    TextView beautician_home_display_name;
    @BindView(R.id.beautician_home_phone_number)
    TextView beautician_home_phone_number;
    @BindView(R.id.beautician_home_email_address)
    TextView beautician_home_email_address;
    @BindView(R.id.beautician_home_profile_image)
    CircleImageView beautician_home_profile_image;

    FirebaseAuth auth;

    private TextView user_full_name, user_email_address, user_phone_number, user_salonName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs);
        ButterKnife.bind(this);

        auth = FirebaseAuth.getInstance();

        if (auth != null) {
            String currentUserID = auth.getUid();
            if (currentUserID != null) {
                FirebaseDatabase.getInstance().getReference().child("Beauticians")
                        .child(currentUserID).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String display_name = String.valueOf(dataSnapshot.child("name").getValue());
                        String email_address = String.valueOf(dataSnapshot.child("email").getValue());
                        String phone_number = String.valueOf(dataSnapshot.child("phoneNumber").getValue());
                        String profile_image = String.valueOf(dataSnapshot.child("profileImage").getValue());

//                if (!(displayName.isEmpty() && emailAddress.isEmpty() && phoneNumber.isEmpty() && profileImage.isEmpty()) ){

                        beautician_home_display_name.setText(display_name);
                        beautician_home_email_address.setText(email_address);
                        beautician_home_phone_number.setText(phone_number);
                        Picasso.get().load(profile_image).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_user).fit().into(beautician_home_profile_image, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(Exception e) {
                                Picasso.get().load(profile_image).placeholder(R.drawable.default_user).fit().into(beautician_home_profile_image);
                            }
                        });
//                }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        tabLayout = findViewById(R.id.tab_layout_id);
        appBarLayout = findViewById(R.id.appbarid);
        viewPager = findViewById(R.id.viewpager_id);



        CustomerViewPagerAdapter viewPagerAdapter = new CustomerViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(new BeauticianHomeFragment(), "HOME");
        viewPagerAdapter.addFragment(new PendingJobsFragment(), "PENDING");
        viewPagerAdapter.addFragment(new UpcomingJobsFragment(), "UPCOMING");
        viewPagerAdapter.addFragment(new PastJobsFragment(), "PAST");

        //set up adapter
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.jobs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_beautician_logout) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(BeauticianHomeActivity.this, BeauticianLoginActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(BeauticianHomeActivity.this, ProfileActivity.class));
        }
 else if (id == R.id.nav_settings) {
            startActivity(new Intent(BeauticianHomeActivity.this, BeauticianSettingsActivity.class));

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_emergency) {
            startActivity(new Intent(BeauticianHomeActivity.this, EmergencyContactActivity.class));
        }

     else if (id == R.id.nav_logout) {
        FirebaseAuth.getInstance().signOut();
    }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
