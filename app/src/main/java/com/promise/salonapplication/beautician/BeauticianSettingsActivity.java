package com.promise.salonapplication.beautician;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.promise.salonapplication.Adapters.CategoryAdapterInAddService;
import com.promise.salonapplication.Adapters.ServiceWithCategory;
import com.promise.salonapplication.Common.Beautician;
import com.promise.salonapplication.Common.Common;
import com.promise.salonapplication.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.angmarch.views.NiceSpinner;
import org.angmarch.views.OnSpinnerItemSelectedListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class BeauticianSettingsActivity extends AppCompatActivity {

    @BindView(R.id.settings_spinner)
    NiceSpinner settings_spinner;

    @BindView(R.id.recycler_salon)
    RecyclerView recycler_salon;

    @BindView(R.id.nameOfSalon)
    TextInputEditText nameOfSalon;

    @BindView(R.id.price)
    TextInputEditText price;

    @BindView(R.id.duration)
    TextInputEditText duration;

    @BindView(R.id.service)
    TextInputEditText service;


    List<String> categoryItem = new ArrayList<>();

    @BindView(R.id.beautician_settings_email)
    EditText beautician_settings_email;
    @BindView(R.id.beautician_settings_full_name)
    EditText beautician_settings_full_name;
    @BindView(R.id.beautician_settings_phone_number)
    EditText beautician_settings_phone_number;

    @BindView(R.id.beautician_settings_salon_name)
    EditText beautician_settings_salon_name;

    @BindView(R.id.beautician_settings_save_changes_button)
    CardView beautician_settings_save_changes_button;

    @BindView(R.id.beautician_settings_cancel)
    CardView beautician_settings_cancel;

    @BindView(R.id.add_service_linear_layout)
    LinearLayout add_service_linear_layout;
    @BindView(R.id.card)
    CardView card;

    @BindView(R.id.beautician_settings_chooseImage_button)
    Button beautician_settings_chooseImage_button;

    @BindView(R.id.beautician_settings_profile_image)
    CircleImageView beautician_settings_profile_image;

    private static final int PICK_IMAGE_REQUEST = 34;
    private Uri filePath;

        String full_name, email_address, phone_number, salon_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beautician_settings);
            ButterKnife.bind(this);
        full_name = beautician_settings_full_name.getText().toString();
         email_address = beautician_settings_email.getText().toString();
         phone_number = beautician_settings_phone_number.getText().toString();
         salon_name = beautician_settings_salon_name.getText().toString();


         beautician_settings_cancel.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 startActivity(new Intent(BeauticianSettingsActivity.this, BeauticianHomeActivity.class));
                 finish();
             }
         });

         card.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 add_service_linear_layout.setVisibility(View.VISIBLE);
             }
         });

         card.setOnLongClickListener(new View.OnLongClickListener() {
             @Override
             public boolean onLongClick(View v) {
                 add_service_linear_layout.setVisibility(View.GONE);

                 return true;
             }
         });




        String category = "Hair";

        CollectionReference collectionReference = FirebaseFirestore.
                getInstance()
                .collection("Service")
                .document("Category")
                .collection(category);

        categoryItem.add("Hair");
        categoryItem.add("Nail");
        categoryItem.add("SkinCare");
        settings_spinner.attachDataSource(categoryItem);

        CategoryAdapterInAddService categoryAdapter = new CategoryAdapterInAddService(this, categoryItem);
        recycler_salon.setAdapter(categoryAdapter);
        recycler_salon.setVisibility(View.VISIBLE);

        settings_spinner.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener() {
            @Override
            public void onItemSelected(NiceSpinner parent, View view, int position, long id) {
                if(position > 0) {

                    parent.getItemAtPosition(position).toString();
                }
                else{
                    recycler_salon.setVisibility(View.GONE);
                }
            }
        });


// TODO: 7/1/19 If app clashes, check on OkHttp3 dependency

        Common.uniqueBeauticianReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String display_name = String.valueOf(dataSnapshot.child("displayName").getValue());
                String email_address = String.valueOf(dataSnapshot.child("emailAddress").getValue());
                String phone_number = String.valueOf(dataSnapshot.child("phoneNumber").getValue());
                String profile_image = String.valueOf(dataSnapshot.child("profileImage").getValue());
                String salon_name = String.valueOf(dataSnapshot.child("nameOfSalon").getValue());


                beautician_settings_full_name.setText(display_name);
                beautician_settings_email.setText(email_address);
                beautician_settings_phone_number.setText(phone_number);
                beautician_settings_salon_name.setText(salon_name);

                Picasso.get().load(profile_image).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_user).fit().into(beautician_settings_profile_image, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(profile_image).placeholder(R.drawable.default_user).fit().into(beautician_settings_profile_image);
                    }
                });
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        beautician_settings_chooseImage_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        beautician_settings_save_changes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile();
                addBeautician();addService();


            }
        });
    }

    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }


    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                beautician_settings_profile_image.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadFile() {
        //checking if file is available
        Common.auth = FirebaseAuth.getInstance();
        final String STORAGE_PATH_UPLOADS = "Beauticians/" + Common.getUserID();
        if (filePath != null) {
            //displaying progress dialog while image is uploading
            final ProgressDialog progressDialog = new ProgressDialog(BeauticianSettingsActivity.this);
            progressDialog.setTitle("Uploading");
            progressDialog.show();

            //getting the storage reference
            StorageReference sRef = Common.storageReference.child(STORAGE_PATH_UPLOADS + System.currentTimeMillis() + "." + getFileExtension(filePath));

            //adding the file to reference
            sRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //dismissing the progress dialog


                            //displaying success toast
                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();

                            Task<Uri> uriTask = sRef.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    return sRef.getDownloadUrl();
                                }

                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    if(task.isSuccessful())
                                    {
                                        Uri downloadUrl = Uri.parse(String.valueOf(task.getResult()));


                                        HashMap<String, String> custom = new HashMap<>();
                                        custom.put("displayName", beautician_settings_full_name.getText().toString());
                                        custom.put("phoneNumber", beautician_settings_phone_number.getText().toString());
                                        custom.put("profileImage", downloadUrl.toString());
                                        custom.put("emailAddress", beautician_settings_email.getText().toString());

                                        Common.uniqueBeauticianReference().addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                dataSnapshot.getRef().setValue(custom);
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });


                                        Log.d("URL", downloadUrl.toString());

                                    }
                                }


                            });
                            progressDialog.dismiss();
                            Intent intent = new Intent(BeauticianSettingsActivity.this, BeauticianHomeActivity.class);
                            startActivity(intent);

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying the upload progress
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        } else {
            startActivity(new Intent(BeauticianSettingsActivity.this,BeauticianHomeActivity.class));
        }
    }

    public void addService() {
        DocumentReference reference = FirebaseFirestore.getInstance().collection("Services").document("Category").collection(settings_spinner.getSelectedItem().toString()).document();

        reference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                    ServiceWithCategory serviceWithCategory = new ServiceWithCategory(
                            service.getText().toString(),
                            duration.getText().toString(),
                            settings_spinner.getSelectedItem().toString(),
                            nameOfSalon.getText().toString(),
                            Double.valueOf(price.getText().toString())

                    );

                    reference.set(serviceWithCategory);
            }
        });

    }


    public void addBeautician() {
        Beautician beautician = new Beautician();
        beautician.setEmail(email_address);
        beautician.setNameOfSalon(salon_name);
        beautician.setName(full_name);
        beautician.setPhoneNumber(phone_number);

        CollectionReference collectionReference = FirebaseFirestore.getInstance().collection("Beauticians");
        collectionReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (!task.getResult().isEmpty()) {


                    collectionReference.add(beautician);

                }
            }
        });
    }
}
