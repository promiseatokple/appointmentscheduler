package com.promise.salonapplication.beautician;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.promise.salonapplication.Adapters.CategoryAdapterInAddService;
import com.promise.salonapplication.Adapters.ServiceWithCategory;
import com.promise.salonapplication.R;

import org.angmarch.views.NiceSpinner;
import org.angmarch.views.OnSpinnerItemSelectedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddServiceActivity extends AppCompatActivity {

    @BindView(R.id.spinner)
    NiceSpinner spinner;

    @BindView(R.id.recycler_salon)
    RecyclerView recycler_salon;

    @BindView(R.id.nameOfSalon)
    TextInputEditText nameOfSalon;

    @BindView(R.id.price)
    TextInputEditText price;

    @BindView(R.id.duration)
    TextInputEditText duration;

    @BindView(R.id.service)
    TextInputEditText service;


    List<String> categoryItem = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_service_layout);
        ButterKnife.bind(this);

        CollectionReference collectionReference = FirebaseFirestore.getInstance().collection("Service");

        categoryItem.add("Hair");
        categoryItem.add("Nail");
        categoryItem.add("Skin Care");
        spinner.attachDataSource(categoryItem);

        CategoryAdapterInAddService categoryAdapter = new CategoryAdapterInAddService(this, categoryItem);
        recycler_salon.setAdapter(categoryAdapter);
        recycler_salon.setVisibility(View.VISIBLE);

        spinner.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener() {
            @Override
            public void onItemSelected(NiceSpinner parent, View view, int position, long id) {
                if(position > 0) {

                    parent.getItemAtPosition(position).toString();
                }
                else{
                    recycler_salon.setVisibility(View.GONE);
                }
            }
        });


                collectionReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (!task.getResult().isEmpty()){
                            ServiceWithCategory serviceWithCategory = new ServiceWithCategory(
                                   service.getText().toString(),
                                    duration.getText().toString(),
                                    spinner.getSelectedItem().toString(),
                                    nameOfSalon.getText().toString(),
                                    Double.valueOf(price.getText().toString())

                            );

                            collectionReference.add(serviceWithCategory);

                            startActivity(new Intent(AddServiceActivity.this, BeauticianHomeActivity.class));




                        }
                    }
                });
            }


}
