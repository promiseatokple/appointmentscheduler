package com.promise.salonapplication.beautician;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.promise.salonapplication.Common.Beautician;
import com.promise.salonapplication.Common.Common;
import com.promise.salonapplication.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BeauticianRegisterActivity extends AppCompatActivity {

//region instances
    ProgressDialog progressDialog;

    @BindView(R.id.customer_register_page_full_name)
    EditText customer_register_page_full_name;

    @BindView(R.id.customer_register_page_email)
    EditText customer_register_page_email;

    @BindView(R.id.customer_register_page_phone_number)
    EditText customer_register_page_phone_number;

    @BindView(R.id.customer_register_page_password)
    EditText customer_register_page_password;

    @BindView(R.id.customer_register_page_salon_name)
    EditText customer_register_page_salon_name;

    @BindView(R.id.customer_register_page_sign_up_button)
    Button customer_register_page_sign_up_button;

    @BindView(R.id.customer_register_page_sign_in_button)
    Button customer_register_page_sign_in_button;


    @BindView(R.id.customer_register_page_progressBar)
    ProgressBar customer_register_page_progressBar;


//endregion

    public String full_name, email_address, phone_number, password, salon_name;
    String user_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beautician_register);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Creating New Account");
        progressDialog.setCancelable(false);


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TOKEN ERROR: ", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        user_token = task.getResult().getToken();

                        // Log and toast
                        String msg = String.valueOf(user_token);
                        Log.d("TOKEN ", msg);
                        Toast.makeText(BeauticianRegisterActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });


        progressDialog.setMessage("Account is being created");


        customer_register_page_sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(BeauticianRegisterActivity.this, BeauticianLoginActivity.class);
//                loginIntent.putExtra("user_email_address", Common.getUserEmail());
                startActivity(loginIntent);
                Toast.makeText(BeauticianRegisterActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                finish();
            }
        });


        customer_register_page_sign_up_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Get instances of the fields
                full_name = customer_register_page_full_name.getText().toString().trim();
                email_address = customer_register_page_email.getText().toString().trim();
                phone_number = customer_register_page_phone_number.getText().toString().trim();
                password = customer_register_page_password.getText().toString().trim();
                salon_name = customer_register_page_salon_name.getText().toString().trim();

                if (TextUtils.isEmpty(email_address)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(full_name)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }

                else {
                    progressDialog.show();

                    //Create user here
                    Common.auth.createUserWithEmailAndPassword(email_address, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Toast.makeText(BeauticianRegisterActivity.this, "Account for " + full_name + " has been created", Toast.LENGTH_LONG).show();
                                    saveEntriesToDatabase();
                                    progressDialog.setMessage("Account has been created");
                                    progressDialog.dismiss();


                                    Common.beauticiansReference().child(Common.getUserID()).child("deviceToken")
                                            .setValue(Common.deviceToken)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {

                                                }
                                            });

                                    Intent registerIntent = new Intent(BeauticianRegisterActivity.this, BeauticianLoginActivity.class);
                                    registerIntent.putExtra("full_name", full_name);
                                    registerIntent.putExtra("emailAddress", email_address);
                                    registerIntent.putExtra("phoneNumber", phone_number);
                                    startActivity(registerIntent);
                                    registerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    finish();



                                    if (!task.isSuccessful()) {
                                        Toast.makeText(BeauticianRegisterActivity.this, "Account for creation failed", Toast.LENGTH_LONG).show();

                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("REGISTRATION_ERROR: ", e.getMessage());
                            Toast.makeText(BeauticianRegisterActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });


    }

    // TODO: 6/28/19 Save data to database not implemented
    public void saveEntriesToDatabase(){
        Beautician beautician = new Beautician();
        beautician.setEmail(email_address);
        beautician.setDeviceToken(user_token);
        beautician.setName(full_name);
        beautician.setPhoneNumber(phone_number);
        beautician.setNameOfSalon(salon_name);
        beautician.setProfileImage("default.png");
        Common.uniqueBeauticianReference().setValue(beautician).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.e("CUSTOMER OBJECT", String.valueOf(beautician));
            }
        });

        DocumentReference documentReference = FirebaseFirestore.getInstance().collection("Beauticians").document(Common.getUserID());
        documentReference.set(beautician).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(BeauticianRegisterActivity.this, "Added Successfully", Toast.LENGTH_SHORT).show();

            }
        });
    }



}
