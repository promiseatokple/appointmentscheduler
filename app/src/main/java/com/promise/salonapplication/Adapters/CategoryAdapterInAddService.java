package com.promise.salonapplication.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.promise.salonapplication.R;

import org.angmarch.views.NiceSpinner;
import org.angmarch.views.OnSpinnerItemSelectedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CategoryAdapterInAddService extends RecyclerView.Adapter<CategoryAdapterInAddService.ViewHolder>{

   List<String> categoryItem = new ArrayList<>();
   @BindView(R.id.spinner)
    NiceSpinner spinner;

   @BindView(R.id.recycler_salon)
   RecyclerView recycler_salon;

   CollectionReference collectionReference;

    public Context context;
    public List<String> categories;

    public CategoryAdapterInAddService(Context context, List<String> categories) {
        this.context = context;
        this.categories = categories;
        categoryItem.add("Hair");
        categoryItem.add("Nail");
        categoryItem.add("Skin Care");


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_category, parent, false);
        collectionReference = FirebaseFirestore.getInstance().collection("Service");
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String category = categories.get(position);

//        holder.category.setText(category.getName());
        spinner.attachDataSource(this.categoryItem);
        spinner.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener() {
            @Override
            public void onItemSelected(NiceSpinner parent, View view, int position, long id) {
                if(position > 0) {

                    parent.getItemAtPosition(position).toString();
                }
                else{
                    recycler_salon.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView category;
        public NiceSpinner spinner;

        public ViewHolder(View itemView) {
            super(itemView);
            spinner = itemView.findViewById(R.id.spinner);
            category = (TextView) itemView.findViewById(R.id.category);
        }
    }



}
