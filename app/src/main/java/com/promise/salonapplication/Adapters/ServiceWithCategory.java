package com.promise.salonapplication.Adapters;

public class ServiceWithCategory {
    String name;
    String duration;
    double price;
    String category;
    String salonName;

    public ServiceWithCategory() {
    }

    public ServiceWithCategory(String name, String duration,  String category, String salonName, double price) {
        this.name = name;
        this.duration = duration;
        this.price = price;
        this.category = category;
        this.salonName = salonName;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSalonName() {
        return salonName;
    }

    public void setSalonName(String salonName) {
        this.salonName = salonName;
    }
}
