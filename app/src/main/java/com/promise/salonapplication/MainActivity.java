package com.promise.salonapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.promise.salonapplication.beautician.BeauticianLoginActivity;
import com.promise.salonapplication.customer.CustomerLoginActivity;

// TODO: 6/28/19 Start application here
public class MainActivity extends AppCompatActivity {

    private static final String CHANNEL_ID = "123";
    //    BindView(R.id.main_page_provider_button)
    Button main_page_provider_button;

//    BindView(R.id.main_page_customer_button)
    Button main_page_customer_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        ButterKnife.bind(this);

        main_page_customer_button = findViewById(R.id.main_page_customer_button);
        main_page_provider_button = findViewById(R.id.main_page_provider_button);

        main_page_customer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* if(Common.getUserID() != null){

                }*/
//
//                NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this, CHANNEL_ID)
//                        .setSmallIcon(R.drawable.ic_notification)
//                        .setContentTitle("Title Here")
//                        .setContentText("Content goes here")
//                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);


/*                // Create an explicit intent for an Activity in your app
                Intent intent = new Intent(MainActivity.this, CustomerSettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, 0);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Set the intent that will fire when the user taps the notification
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true);


                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.this);

// notificationId is a unique int for each notification that you must define
                notificationManager.notify(notificationId, builder.build());*/







//                myAppsNotificationManager.registerNotificationChannel("NEWS", "News", "News Descritpo");
//                myAppsNotificationManager.triggerNotification(MainActivity.class, String.valueOf(R.string.NEWS_CHANNEL_ID), "You are about to confirm ", "Going on to confirm", "no big text here", 1, true, 1);
////                startActivity(intent);
                Intent goToCustomerLoginActivity = new Intent(MainActivity.this, CustomerLoginActivity.class);
                goToCustomerLoginActivity.putExtra("whoami", "customer");
                startActivity(goToCustomerLoginActivity);
                finish();
            }


        });

        main_page_provider_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToBeauticianLoginActivity = new Intent(MainActivity.this, BeauticianLoginActivity.class);
                goToBeauticianLoginActivity.putExtra("whoami", "beautician");
                startActivity(goToBeauticianLoginActivity);
                finish();
            }
        });



    }



}
