package com.promise.salonapplication.Common;

public class ServiceModel {
    public String serviceName;
    public int servicePrice;
    public int serviceDuration;



    public ServiceModel() {
    }

    public ServiceModel(String serviceName, int servicePrice, int serviceDuration) {
        this.serviceName = serviceName;
        this.servicePrice = servicePrice;
        this.serviceDuration = serviceDuration;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(int servicePrice) {
        this.servicePrice = servicePrice;
    }

    public int getServiceDuration() {
        return serviceDuration;
    }

    public void setServiceDuration(int serviceDuration) {
        this.serviceDuration = serviceDuration;
    }

}
