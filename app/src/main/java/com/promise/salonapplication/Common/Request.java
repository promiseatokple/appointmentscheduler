package com.promise.salonapplication.Common;

public class Request {
    public String customerName;

    public Request(String customerName) {
        this.customerName = customerName;
    }

    public Request() {
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
