package com.promise.salonapplication.Common;

public class BookingInformation {


    private String   customerName;
    private String customerPhone;
    private String time;

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    private String serviceCategory;
    private String chosenService;
    private String priceOfService;
    private String serviceDuration;

    public BookingInformation(){}

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getChosenService() {
        return chosenService;
    }

    public void setChosenService(String chosenService) {
        this.chosenService = chosenService;
    }

    public String getPriceOfService() {
        return priceOfService;
    }

    public void setPriceOfService(String priceOfService) {
        this.priceOfService = priceOfService;
    }

    public String getServiceDuration() {
        return serviceDuration;
    }

    public void setServiceDuration(String serviceDuration) {
        this.serviceDuration = serviceDuration;
    }
}
