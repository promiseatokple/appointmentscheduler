package com.promise.salonapplication.Common;

public class Service {
    String serviceId;
    String name;
    String duration;
    double price;

    public Service() {
    }

    public Service(String name, String duration, double price) {
        this.name = name;
        this.duration = duration;
        this.price = price;
    }

    public Service(String serviceId, String name, String duration, double price) {
        this.serviceId = serviceId;
        this.name = name;
        this.duration = duration;
        this.price = price;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
