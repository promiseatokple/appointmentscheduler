package com.promise.salonapplication.Common;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.promise.salonapplication.R;
import com.promise.salonapplication.customer.ChooseBeauticianActivity;
import com.promise.salonapplication.customer.multiselect.SalonServiceOld;
import com.promise.salonapplication.services.MyAppsNotificationManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.ProgressLayout;
import dmax.dialog.SpotsDialog;

public class OrderActivity extends AppCompatActivity {
//    @BindView(R.id.progress_bar)
//    ProgressBar progressBar;

    @BindView(R.id.order_recycler)
    RecyclerView recyclerView;

    @BindView(R.id.order_items_count)
    TextView order_items_count;

    @BindView(R.id.order_price_total)
    TextView order_price_total;

    @BindView(R.id.choose_beautician)
    Button choose_beautician;

    @BindView(R.id.order_load_progress)
    ProgressLayout order_load_progress;

    Intent send;

    private FirebaseDatabase database;
    private FirebaseRecyclerAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    MyAppsNotificationManager myAppsNotificationManager;
    AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        myAppsNotificationManager = new MyAppsNotificationManager(this);

        alertDialog = new  SpotsDialog.Builder().setContext(OrderActivity.this).setMessage("Retrieving Selected Services").setCancelable(true).setCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                android.support.v7.app.AlertDialog.Builder dialog1 = new android.support.v7.app.AlertDialog.Builder(OrderActivity.this);
                dialog1.setTitle("Continue Booking").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(OrderActivity.this, ChooseBeauticianActivity.class));

                        Log.d("Postive :", "Ok clicked");
                    }
                }).setNegativeButton("Cancel Request", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("Negative :", "Cancel clicked");
                        startActivity(new Intent(OrderActivity.this, OrderActivity.class));

                    }
                }).setMessage("This process is taking longer than expected\n I'm sure I have selected the right services. Continue Booking" );

                dialog1.create();
                dialog1.show();
            }
        }).build();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

        myAppsNotificationManager.registerNotificationChannel(
                getString(R.string.NEWS_CHANNEL_ID),
                getString(R.string.CHANNEL_NEWS),
                getString(R.string.CHANNEL_DESCRIPTION));

        send = new Intent();

        database = FirebaseDatabase.getInstance();

        // TODO: 6/15/19 Work on this pending intent
//        Intent intent = new Intent(this, CustomerMapsActivity.class);
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        ButterKnife.bind(this);
        init();
        getBeauticians();

        choose_beautician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 send = new Intent(OrderActivity.this, ChooseBeauticianActivity.class);
                //intent1.putExtra("")
                startActivity(send);
            }
        });


        DatabaseReference firebaseDatabase =   FirebaseDatabase.getInstance().getReference().child("Customers").child(Common.getUserID()).child("Cart");

        firebaseDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                 Long count = dataSnapshot.getChildrenCount();

                 String service_name = String.valueOf(dataSnapshot.child("name").getValue());
                List<String>  serviceList = new ArrayList<>();

                Long price = (long) dataSnapshot.child("price").getValue();
                order_items_count.setText(String.valueOf(count));

                for(int i = 0; i<count; i++){
                    price =+ Long.valueOf(String.valueOf(dataSnapshot.child("price").getValue()));
                }

                for(int i = 0; i<count; i++){
                    service_name = String.valueOf(new StringBuilder().append(String.valueOf(dataSnapshot.child("name").getValue())));
                    serviceList.add(service_name);
                    send.putExtra("services", (Parcelable) serviceList);
                    Log.d("Services:" , serviceList.toString());
                }

                Log.d("ITEMS:", String.valueOf(count));

                double price1 = Double.valueOf(String.valueOf(dataSnapshot.child("price").getValue()));
                double duration = Integer.valueOf(String.valueOf(dataSnapshot.child("duration").getValue()));


                    Log.d("Price: ", String.valueOf(price));


                order_price_total.setText(String.valueOf(new StringBuilder("GHC").append(price)));
//                order_price_total.setText(String.valueOf(price));


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Long count = dataSnapshot.getChildrenCount();
                order_items_count.setText(String.valueOf(count));

                Log.d("ITEMS:", String.valueOf(count));


                double price = Double.valueOf(String.valueOf(dataSnapshot.child("price").getValue()));
                double duration = Integer.valueOf(String.valueOf(dataSnapshot.child("duration").getValue()));

                order_price_total.setText(String.valueOf(price));
//                order_price_total.setText(String.valueOf(price));
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                Long count = dataSnapshot.getChildrenCount();

                Log.d("ITEMS:", String.valueOf(count));


                order_items_count.setText(String.valueOf(count));
                double price = Double.valueOf(String.valueOf(dataSnapshot.child("price").getValue()));
                double duration = Integer.valueOf(String.valueOf(dataSnapshot.child("duration").getValue()));

                for(int i = 0; i< count; i++){
                    price += price;
                    duration += duration;
                }

                order_price_total.setText(String.valueOf(price));
//                order_price_total.setText(String.valueOf(price));
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Long count = dataSnapshot.getChildrenCount();

                Log.d("ITEMS:", String.valueOf(count));


                order_items_count.setText(String.valueOf(count));
                double price = Double.valueOf(String.valueOf(dataSnapshot.child("price").getValue()));
                double duration = Integer.valueOf(String.valueOf(dataSnapshot.child("duration").getValue()));

                for(int i = 0; i< count; i++){
                    price += price;
                    duration += duration;
                }

                order_price_total.setText(String.valueOf(price));
//                order_price_total.setText(String.valueOf(price));


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        firebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void init(){
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void getBeauticians(){
//        Query query = db.collection("Beauticians");
        Query query1  = database.getReference().child("Customers").child(Common.getUserID()).child("Cart");


        FirebaseRecyclerOptions<SalonServiceOld> firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<SalonServiceOld>()
                .setQuery(query1, SalonServiceOld.class).build();

        adapter = new FirebaseRecyclerAdapter<SalonServiceOld, OrdersHolder>(firebaseRecyclerOptions) {
            @Override
            public void onBindViewHolder(@NonNull OrdersHolder holder, int position, @NonNull SalonServiceOld model) {
                //progressBar.setVisibility(View.GONE);
                alertDialog.dismiss();

//
                holder.tv_service_duration.setText(String.valueOf(model.getDuration()));

                holder.tv_service_name.setText(model.getName());
                holder.tv_service_price.setText(String.valueOf(model.getPrice()));









            }

            @NonNull
            @Override
            public OrdersHolder onCreateViewHolder(@NonNull ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.single_order, group, false);




                return new OrdersHolder(view);
            }

        };

        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }




    public class OrdersHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvServiceName)
        TextView tv_service_name;
        @BindView(R.id.tvServiceDuration)
        TextView tv_service_duration;
        @BindView(R.id.tvServicePrice)
        TextView tv_service_price;






        public OrdersHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }




}
