package com.promise.salonapplication.Common;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Common {

    public static Calendar bookingDate = Calendar.getInstance();
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy");

    public static FirebaseAuth auth = FirebaseAuth.getInstance();
    public static FirebaseUser firebaseUser = auth.getCurrentUser();
    public static DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    public static FirebaseDatabase database = FirebaseDatabase.getInstance();
    public static StorageReference storageReference = FirebaseStorage.getInstance().getReference();



//  public static DatabaseReference userBooking = FirebaseDatabase.getInstance().getReference().child("Customers").child(getUserID()).child("Cart").push();



    public static FirebaseUser getCurrentUser() {

        return firebaseUser;

    }

    public static String getUserEmail() {

        return firebaseUser.getEmail();
    }

    //name = ((city == null) || (city.getName() == null) ? "N/A" : city.getName());
    public static String getUserID() {
        if (auth != null) {
            String user_id = auth.getUid();
            if (user_id != null) {

                return auth.getUid();
            }

            else {
                return FirebaseAuth.getInstance().getUid();
            }

        }

        else
            return FirebaseAuth.getInstance().getUid();


    }

    public static DatabaseReference requests(String beautician_id){
        return databaseReference.child("Beauticians").child(beautician_id);
    }

    public static DatabaseReference customersReference() {
        return databaseReference.child("Customers");
    }

    public static DatabaseReference uniqueCustomerReference() {

        return databaseReference.child("Customers").child(getUserID());

    }

    public static String deviceToken = FirebaseInstanceId.getInstance().getToken();

    public static DatabaseReference beauticiansReference() {
        return databaseReference.child("Beauticians");
    }

    public static DatabaseReference uniqueBeauticianReference() {

        return databaseReference.child("Beauticians").child(getUserID());


    }

    public static DatabaseReference bookingsDatabase() {

        return databaseReference.child("Bookings");


    }



    public static DatabaseReference friendsDatabase(){
        return databaseReference.child("Friends");
    }
    public static DatabaseReference notificationDatabase(){
        return databaseReference.child("notifications");
    }



    public static String currentDate = DateFormat.getDateInstance().format(new Date());

    public void saveCategoryToDatabase(String category) {
        BookingInformation info = new BookingInformation();
        info.setServiceCategory(category);

        Common.uniqueBeauticianReference().setValue(info).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.e("CUSTOMER OBJECT", String.valueOf(info));
            }
        });

    }

    public void updateAndAddServiceDetails(String category) {
        BookingInformation info = new BookingInformation();
        info.setServiceCategory(category);

        Common.uniqueBeauticianReference().setValue(info).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                Log.e("CUSTOMER OBJECT", String.valueOf(info));
            }
        });


        HashMap<String, String> custom = new HashMap<>();
//        custom.put("displayName", customer_settings_full_name.getText().toString());
//        custom.put("phoneNumber", customer_settings_phone_number.getText().toString());
//        custom.put("profileImage", downloadUrl.toString());
//        custom.put("emailAddress", customer_settings_email.getText().toString());

        Common.uniqueBeauticianReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().setValue(custom);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }



    public static void saveBookingInfoToDatabase(String name, String price) {
     ServicesSummary summary = new ServicesSummary();
     summary.setServiceName(name);
     summary.setServicePrice(price);

        if (auth != null) {
            String currentUserID = auth.getUid();
            if (currentUserID != null) {
                FirebaseDatabase.getInstance().getReference().child("Customers")
                        .child(currentUserID).child("Orders").push().setValue(summary).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.e("BEAUTICIAN OBJECT", ""

                        );
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Error:", e.getMessage());
                    }
                });
            }
        }



    }

}
