package com.promise.salonapplication.Common;

import android.os.Parcel;
import android.os.Parcelable;

public class Beautician implements Parcelable {

    private String name;
    private String nameOfSalon;
    private String email;
    private String phoneNumber;
    private String profileImage;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public static Creator<Beautician> getCREATOR() {
        return CREATOR;
    }

    public String deviceToken;


    public Beautician(Parcel in) {

        name = in.readString();
        nameOfSalon = in.readString();
        email = in.readString();
        phoneNumber = in.readString();
        profileImage = in.readString();
    }

    public static final Creator<Beautician> CREATOR = new Creator<Beautician>() {
        @Override
        public Beautician createFromParcel(Parcel in) {
            return new Beautician(in);
        }

        @Override
        public Beautician[] newArray(int size) {
            return new Beautician[size];
        }
    };

    public Beautician() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(name);
        dest.writeString(nameOfSalon);
        dest.writeString(email);
        dest.writeString(phoneNumber);
        dest.writeString(profileImage);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameOfSalon() {
        return nameOfSalon;
    }

    public void setNameOfSalon(String nameOfSalon) {
        this.nameOfSalon = nameOfSalon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
