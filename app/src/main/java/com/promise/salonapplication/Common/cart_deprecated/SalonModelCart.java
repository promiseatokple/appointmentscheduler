package com.promise.salonapplication.Common.cart_deprecated;

import com.promise.salonapplication.Common.ServiceModel;

import java.util.ArrayList;

public class SalonModelCart {
    private ArrayList<ServiceModel> cartItems = new ArrayList<>();
    public ServiceModel getServices(int position){
        return cartItems.get(position);
    }
    public void setServices(ServiceModel service){
        cartItems.add(service);
    }
    public int getCartSize(){

        return cartItems.size();
    }
    public boolean CheckServiceInCart(ServiceModel serviceModel){
        return cartItems.contains(serviceModel);
    }
}

