package com.promise.salonapplication.Common;

public class BeauticianModel {
    public  String display_name;
    public  String email_address;
    public  String phone_number;
    public  boolean status;
    public String salon_name;
    public String start_hours;
    public String end_hours;
    public  ServiceModel[] services;

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String profile_image;


    public BeauticianModel() {
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getSalon_name() {
        return salon_name;
    }

    public void setSalon_name(String salon_name) {
        this.salon_name = salon_name;
    }

    public String getStart_hours() {
        return start_hours;
    }

    public void setStart_hours(String start_hours) {
        this.start_hours = start_hours;
    }

    public String getEnd_hours() {
        return end_hours;
    }

    public void setEnd_hours(String end_hours) {
        this.end_hours = end_hours;
    }

    public ServiceModel[] getServices() {
        return services;
    }

    public void setServices(ServiceModel[] services) {
        this.services = services;
    }
}
