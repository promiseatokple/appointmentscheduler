package com.promise.salonapplication.Common;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class CustomerModel {

    public String displayName;
    public String emailAddress;
    public String phoneNumber;
    public String profileImage;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String deviceToken;

    public CustomerModel() {
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("displayName", displayName);
        result.put("emailAddress", emailAddress);
        result.put("phoneNumber", phoneNumber);
        result.put("profileImage", profileImage);


        return result;

    }
}