package com.promise.salonapplication.Common;

import com.google.firebase.Timestamp;

import java.util.Date;

public class RequestInformation {

    public Timestamp timestamp;
    public CustomerModel customer;
    public Date date;
    public ServiceModel serviceModel;


    public RequestInformation() {
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }


    public ServiceModel getServiceModel() {
        return serviceModel;
    }

    public void setServiceModel(ServiceModel serviceModel) {
        this.serviceModel = serviceModel;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
