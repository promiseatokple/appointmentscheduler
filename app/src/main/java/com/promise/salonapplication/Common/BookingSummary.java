package com.promise.salonapplication.Common;

public class BookingSummary {

    public BookingSummary(){}

    public String beauticianName,beauticianPhoneNumber, beauticianEmailAddress;
    public Long price;
    public String[] services;

    public String getBeauticianName() {
        return beauticianName;
    }

    public void setBeauticianName(String beauticianName) {
        this.beauticianName = beauticianName;
    }

    public String getBeauticianPhoneNumber() {
        return beauticianPhoneNumber;
    }

    public void setBeauticianPhoneNumber(String beauticianPhoneNumber) {
        this.beauticianPhoneNumber = beauticianPhoneNumber;
    }

    public String getBeauticianEmailAddress() {
        return beauticianEmailAddress;
    }

    public void setBeauticianEmailAddress(String beauticianEmailAddress) {
        this.beauticianEmailAddress = beauticianEmailAddress;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String[] getServices() {
        return services;
    }

    public void setServices(String[] services) {
        this.services = services;
    }
}
