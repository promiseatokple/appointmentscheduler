'use strict';
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

/*
exports.sendNotification = functions.database
    .ref(`/notifications/{beautician_id}/{notification_id}`)
    .onWrite(async (change, context) => {
        const beautician_id = context.params.beautician;
        const notification_id = context.params.notification_id;

        console.log("USER ID: ", beautician_id);

        // if (!change.after.val()) {
        //     console.log(" The received notification has been deleted: ", notification_id);
        // }

        //const getDeviceToken = admin.database().ref(`/Customers/${beautician_id}/device_token`);

        // const deviceToken = admin.database().ref(`/Beauticians/${beautician_id}/deviceToken`)
        //     .once('value');

        // const getFollowerProfile = admin.auth().getUser(beautician_id);
/!*        const followerProfile = admin.auth().getUser(beautician_id);

        let tokenSnapshot;
        let tokens;

        const results = await Promise.all([deviceToken, followerProfile]);
        tokenSnapshot = results[0];
        const follower = results[1];

        if(!tokenSnapshot.hasChildren()){

            console.log('There are no notification tokens to send to');

        }

        console.log('There are ', tokenSnapshot.numChildren(), 'tokens to send notifications to');
        console.log('Fetched profile', follower);

        const payload = {
            notification: {
                title: 'New Appointment',
                body: 'You have a new appointment from User',
                icon: 'default'
            }
        };

        tokens = Object.keys(tokenSnapshot.val());
        const response = await admin.messaging().sendToDevice(tokens, payload);

        const tokensToRemove = [];

        response.results.forEach((result, index) => {
            const error = result.error;

            if(error){
                console.log("Failure sending notification to", tokens[index], error);
                if(error.code === 'messaging/invalid-registration-token' ||
                error.code === 'messaging/registration-token-not-registered')
                {
                    tokensToRemove.push(tokenSnapshot.ref.child(tokens[index]).remove());
                }
            }
        });
        return Promise.all(tokensToRemove);*!/
        // return deviceToken.then(result => {
        //
        //     const token_id = result.val();
        //     const payload = {
        //         notification: {
        //             title: 'New Appointment',
        //             body: 'You have a new appointment from User',
        //             icon: 'default'
        //         }
        //     };
        //
        //     return admin.messaging().sendToDevice(token_id, payload)
        //         .then(response => {
        //             console.log("Messaging to device implementation")
        //         });
        //
        // });
        //
        // const dt = admin.database().ref(`/Customers/${beautician_id}/device_token`).once()

    });


*/





exports.sendNotificationToProvider = functions.database
    .ref(`/notifications/{beautician_id}/{notification_id}`)
    .onWrite(((change, context) => {
        const beautician_id = context.params.beautician_id;
        const notification_id = context.params.notification_id;

       // console.log("USER ID: ", beautician_id);

        if (!context.resource.labels) {
           // console.log(" The received notification has been deleted: ", notification_id);
        }

        const fromUser = admin.database().ref(`/notifications/${beautician_id}/${notification_id}`).once('value');
        return fromUser.then(fromUserResult => {
            const whoSentTheRequest = fromUserResult.val().display_name;
            const customerID = fromUserResult.val().customer_id;

            const whatService = fromUserResult.val().service_name;
            //console.log('You have a new notification from ', whoSentTheRequest, 'for', whatService);


            const deviceToken = admin.database().ref(`/Beauticians/${beautician_id}/deviceToken`)
                .once('value');

            return deviceToken.then(result => {

                const token_id = result.val();
                const payload = {
                    notification: {
                        title: 'New Appointment ***',
                        body: `You have a new appointment from ${whoSentTheRequest} for ${whatService}`,
                        icon: 'default',
                        click_action: 'com.promise.salonapplication_TARGET_NOTIFICATION'
                    },
                    data: {
                        'customer_id': customerID
                    }
                };

                return admin.messaging().sendToDevice(token_id, payload)
                    .then(response => {
                        //console.log("Messaging to device implementation")
                    });

            });
        });






        // const dt = admin.database().ref(`/Customers/${beautician_id}/device_token`).once()

    }));




/*exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});*/
